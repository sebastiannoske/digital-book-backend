<?php

use Illuminate\Database\Seeder;

class PageContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page_contents')->insert([
            'slug' => 'impressum',
            'book_id' => 1
        ]);
        DB::table('page_contents')->insert([
            'slug' => 'datenschutz',
            'book_id' => 1
        ]);
        DB::table('page_contents')->insert([
            'slug' => 'copyright',
            'book_id' => 1
        ]);
    }
}
