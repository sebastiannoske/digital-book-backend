<?php

use Illuminate\Database\Seeder;

class ChapterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chapters')->insert([
            'title' => 'Titel - Kapitel 1',
            'introduction' => 'Die Beschäftigung mit dem Thema „Wirtschaftspolitik“ soll mit einem Blick in die ferne Zukunft beginnen. Wie werden Sie, wie werden Ihre Kinder oder Enkelkinder in 20 Jahren, in 50 Jahren, in 100 Jahren leben und wirtschaften?',
            'book_id' => 1
        ]);

        DB::table('chapters')->insert([
            'title' => 'Titel - Kapitel 2   ',
            'introduction' => 'Die Beschäftigung mit dem Thema „Wirtschaftspolitik“ soll mit einem Blick in die ferne Zukunft beginnen. Wie werden Sie, wie werden Ihre Kinder oder Enkelkinder in 20 Jahren, in 50 Jahren, in 100 Jahren leben und wirtschaften?',
            'book_id' => 1
        ]);

        DB::table('chapters')->insert([
            'title' => 'Titel - Kapitel 3',
            'introduction' => 'Die Beschäftigung mit dem Thema „Wirtschaftspolitik“ soll mit einem Blick in die ferne Zukunft beginnen. Wie werden Sie, wie werden Ihre Kinder oder Enkelkinder in 20 Jahren, in 50 Jahren, in 100 Jahren leben und wirtschaften?',
            'book_id' => 1
        ]);

        DB::table('chapters')->insert([
            'title' => 'Titel - Kapitel 4',
            'introduction' => 'Die Beschäftigung mit dem Thema „Wirtschaftspolitik“ soll mit einem Blick in die ferne Zukunft beginnen. Wie werden Sie, wie werden Ihre Kinder oder Enkelkinder in 20 Jahren, in 50 Jahren, in 100 Jahren leben und wirtschaften?',
            'book_id' => 1
        ]);

        DB::table('chapters')->insert([
            'title' => 'Titel - Kapitel 5',
            'introduction' => 'Die Beschäftigung mit dem Thema „Wirtschaftspolitik“ soll mit einem Blick in die ferne Zukunft beginnen. Wie werden Sie, wie werden Ihre Kinder oder Enkelkinder in 20 Jahren, in 50 Jahren, in 100 Jahren leben und wirtschaften?',
            'book_id' => 1
        ]);

        DB::table('chapters')->insert([
            'title' => 'Titel - Kapitel 6',
            'introduction' => 'Die Beschäftigung mit dem Thema „Wirtschaftspolitik“ soll mit einem Blick in die ferne Zukunft beginnen. Wie werden Sie, wie werden Ihre Kinder oder Enkelkinder in 20 Jahren, in 50 Jahren, in 100 Jahren leben und wirtschaften?',
            'book_id' => 1
        ]);
    }
}
