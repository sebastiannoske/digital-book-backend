<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chart_items', function (Blueprint $table) {
            $table->increments('id');
            $table->double('value', 15, 4)->default(0);
            $table->string('color')->default('#BD0907');
            $table->integer('dataset_id')->unsigned()->references('id')->on('chart_datasets')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chart_items');
    }
}
