<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFurtherInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('further_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text')->nullable();
            $table->integer('sort_order')->default(0);
            $table->integer('learning_section_id')->unsigned()->references('id')->on('learning_sections')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('further_informations');
    }
}
