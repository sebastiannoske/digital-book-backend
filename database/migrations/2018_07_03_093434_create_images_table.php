<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->string('image_original_path');
            $table->string('image_path');
            $table->string('image_thumb_path');
            $table->integer('width_original');
            $table->integer('width');
            $table->integer('width_thumb');
            $table->integer('height_original');
            $table->integer('height');
            $table->integer('height_thumb');
            $table->integer('file_size_original');
            $table->integer('file_size');
            $table->integer('file_size_thumb');
            $table->string('size')->default('large');
            $table->integer('content_element_id')->nullable()->unsigned()->references('id')->on('content_elements')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
