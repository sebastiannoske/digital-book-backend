<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLearningSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learning_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('introduction')->nullable();
            $table->boolean('is_learning_section')->default(false);
            $table->boolean('keywords_enabled')->default(true);
            $table->integer('sort_order')->default(0);
            $table->integer('sub_chapter_id')->unsigned()->references('id')->on('sub_chapters')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('learning_sections');
    }
}
