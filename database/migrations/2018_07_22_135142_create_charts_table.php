<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->default('bar');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->integer('y_axis_min')->default(0);
            $table->integer('y_axis_max')->default(0);
            $table->string('label_x_axis')->nullable();
            $table->string('label_y_axis')->nullable();
            $table->integer('content_element_id')->unsigned()->references('id')->on('content_elements')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charts');
    }
}
