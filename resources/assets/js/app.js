
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('edit-content-elements', require('./components/EditContentElements.vue').default);
Vue.component('edit-learning-sections', require('./components/EditLearningSections.vue').default);
Vue.component('edit-subchapters', require('./components/EditSubChapters.vue').default);
Vue.component('edit-chapters', require('./components/EditChapters.vue').default);
Vue.component('file-upload', require('./components/UploadFile.vue').default);
Vue.component('chart-image-upload', require('./components/UploadChartImage.vue').default);
Vue.component('infobox-image-upload', require('./components/UploadInfoboxImage.vue').default);

const app = new Vue({
    el: '#app'
});
