@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="bread-crump">

            <a href="{{ url('/books/' . $book_id ) }}" target="_self">Schulbuch</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id) }}" target="_self">Kapitel</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id ) }}" target="_self">Unterkapitel</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id . '/learning-sections/' . $learning_section_id) }}" target="_self">Abschnit / Lernabschnitt</a> >
            Link zu Medium

        </div>

        <div class="row">

            <div class="col-md-12">



                @if (isset($media_link))

                    <?php $formError = $errors->all() ? true : false; ?>

                    @if ($formError)

                        <p class="alert alert-danger">

                            Es ist ein Fehler im Formular beim Anpassen des Kapitels aufgetreten.

                        </p>

                        <br/><br/><br/>

                    @endif

                    @if (session()->has('message'))

                        <p class="alert alert-success">

                            <?php echo session('message')[0]; ?>

                        </p>

                        <br/><br/><br/>

                    @endif

                        @if (!isset($media_link->type))

                            <h3>Inhaltstyp wählen</h3>

                            <div class="row">

                                <div class="col-md-6">

                                    <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/media-links/$media_link->id/updatetype"]); ?>

                                    <input name="type" type="hidden" value="link">

                                    <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Link</button>

                                    <?php echo Form::close(); ?>

                                </div>

                                <div class="col-md-6">

                                    <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/media-links/$media_link->id/updatetype"]); ?>

                                    <input name="type" type="hidden" value="file">

                                    <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Datei</button>

                                    <?php echo Form::close(); ?>

                                </div>

                            </div>

                        @else

                            @if ($media_link->type === 'file')


                                @if ($media_link->attachment)

                                <h2>Vorhandene Datei</h2>

                                <a href="<?php echo asset('storage/' . $media_link->attachment->filename)?>">{{$media_link->display_name}}</a>

                                <br/><br/>

                                @endif

                                <h2>Dateiupload</h2>

                                <file-upload ></file-upload>

                            @endif

                            <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/media-links/$media_link->id/update"]); ?>

                            @if ($media_link->type === 'link')

                            <div class="form-group  <?php if ($errors->has('url')) echo 'has-error'; ?>">
                                {{ Form::label('url', 'Medium URL')}}
                                {{ Form::text('url', $media_link->url, array_merge(['class' => 'form-control', 'id' => 'url', 'required' => 'required'])) }}
                            </div>

                            @endif

                            <div class="form-group  <?php if ($errors->has('display_name')) echo 'has-error'; ?>">
                                {{ Form::label('display_name', 'Anzeigename')}}
                                {{ Form::text('display_name', $media_link->display_name, array_merge(['class' => 'form-control', 'id' => 'display_name'])) }}
                            </div>

                            <div class="right-align">

                                <button type="submit" class="btn btn-primary">speichern</button>

                            </div>

                            <?php echo Form::close(); ?>

                        @endif

                @else

                    <h3>Sie haben nicht die nötigen Rechte um diese Seite sehen zu können.</h3>

                @endif

            </div>

        </div>

    </div>

@endsection