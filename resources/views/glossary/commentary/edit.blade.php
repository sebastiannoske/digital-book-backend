@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="bread-crump">

            <a href="{{ url('/books/' . $book_id ) }}" target="_self">Schulbuch</a> >
            <a href="{{ url('/books/' . $book_id . '/glossaries/' . $glossary_id) }}" target="_self">Glossar</a> >
            Erklärung

        </div>

        <div class="row">

            <div class="col-md-12">

                @if (isset($commentary))

                    <?php $formError = $errors->all() ? true : false; ?>

                    @if ($formError)

                        <p class="alert alert-danger">

                            Es ist ein Fehler im Formular beim Anpassen des Kapitels aufgetreten.

                        </p>

                        <br/><br/><br/>

                    @endif

                    @if (session()->has('message'))

                        <p class="alert alert-success">

                            <?php echo session('message')[0]; ?>

                        </p>

                        <br/><br/><br/>

                    @endif

                    <h1>Kommentar</h1>

                    {!! Form::open(['url' => "/books/$book_id/glossaries/$glossary_id/commentaries/$commentary->id/update"]) !!}

                    <div class="form-group  <?php if ($errors->has('glosse')) echo 'has-error'; ?>">
                        {{ Form::label('glosse', 'Wort')}}
                        {{ Form::text('glosse', $commentary->glosse, array_merge(['class' => 'form-control', 'id' => 'glosse', 'required' => 'required'])) }}
                    </div>

                    <div class="form-group <?php if ($errors->has('comment')) echo 'has-error'; ?>">
                        {{ Form::label('comment', 'Erklärung')}}
                        {{ Form::textarea('comment', $commentary->comment, array_merge(['class' => 'form-control', 'id' => 'comment', 'rows' => 7, 'required' => 'required'])) }}
                    </div>

                    <div class="form-group <?php if ($errors->has('reference')) echo 'has-error'; ?>">
                        {{ Form::label('reference', 'Bezugsquelle')}}
                        {{ Form::text('reference', $commentary->reference, array_merge(['class' => 'form-control', 'id' => 'reference'])) }}
                    </div>

                    <div class="right-align">

                        <button type="submit" class="btn btn-primary">speichern</button>

                    </div>

                    {!! Form::close() !!}

                @endif

            </div>

        </div>

    </div>

@endsection
