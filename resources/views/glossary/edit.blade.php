@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="bread-crump">

            <a href="{{ url('/books/' . $book_id) }}" target="_self">Schulbuch</a> > Glossar

        </div>

        <div class="row">

            <div class="col-md-12">

                @if (isset($glossary))

                    <?php $formError = $errors->all() ? true : false; ?>

                    @if ($formError)

                        <p class="alert alert-danger">

                            Es ist ein Fehler im Formular beim Anpassen des Glossars aufgetreten.

                        </p>

                        <br/><br/><br/>

                    @endif

                    @if (session()->has('message'))

                        <p class="alert alert-success">

                            <?php echo session('message')[0]; ?>

                        </p>

                        <br/><br/><br/>

                    @endif

                        <h4>Gloassar</h4>

                        <ul class="list-group padding">

                            <div class="row">

                                @foreach ($glossary->commentaries as $commentary)

                                <div class="col-md-6">

                                    <li class="list-group-item disabled">

                                        <div class="row chapter-row">

                                            <div class="col-md-6">

                                                <h5>{{$commentary->glosse}}</h5>

                                            </div>

                                            <div class="col-md-6 right-align">

                                                <a href="{{ url('/books/' . $book_id . '/glossaries/' . $glossary->id . '/commentaries/' . $commentary->id) }}" target="_self"><button type="button" class="btn btn-primary">Bearbeiten</button></a>

                                                <?php echo Form::open(['url' => "/books/$book_id/glossaries/$glossary->id/commentaries/$commentary->id/delete"]); ?>

                                                <button type="submit" class="btn btn-danger">löschen</button>

                                                <?php echo Form::close(); ?>

                                            </div>

                                        </div>

                                    </li>

                                </div>

                                @endforeach

                            </div>

                        </ul>

                        <?php echo Form::open(['url' => "/books/$book_id/glossaries/$glossary->id/commentaries/store"]); ?>

                        <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Inhaltselement hinzufügen</button>

                        <?php echo Form::close(); ?>

                @else

                    <h3>Sie haben nicht die nötigen Rechte um diese Seite sehen zu können.</h3>

                @endif

            </div>

        </div>

    </div>

@endsection
