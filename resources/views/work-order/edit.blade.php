@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="bread-crump">

            <a href="{{ url('/books/' . $book_id ) }}" target="_self">Schulbuch</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id) }}" target="_self">Kapitel</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id ) }}" target="_self">Unterkapitel</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id . '/learning-sections/' . $learning_section_id) }}" target="_self">Abschnit / Lernabschnitt</a> >
            Arbeitsauftrag

        </div>

        <div class="row">

            <div class="col-md-12">

                @if (isset($work_order))

                    <?php $formError = $errors->all() ? true : false; ?>

                    @if ($formError)

                        <p class="alert alert-danger">

                            Es ist ein Fehler im Formular beim Anpassen des Kapitels aufgetreten.

                        </p>

                        <br/><br/><br/>

                    @endif

                    @if (session()->has('message'))

                        <p class="alert alert-success">

                            <?php echo session('message')[0]; ?>

                        </p>

                        <br/><br/><br/>

                    @endif

                    <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/work-orders/$work_order->id/update"]); ?>

                    <div class="form-group  <?php if ($errors->has('text')) echo 'has-error'; ?>">
                        {{ Form::label('text', 'Text')}}
                        {{ Form::textarea('text', $work_order->text, array_merge(['class' => 'form-control summernote', 'id' => 'text', 'rows' => 7, 'required' => 'required'])) }}
                    </div>

                    <div class="right-align">

                        <button type="submit" class="btn btn-primary">speichern</button>

                    </div>

                    <?php echo Form::close(); ?>

                @else

                    <h3>Sie haben nicht die nötigen Rechte um diese Seite sehen zu können.</h3>

                @endif

            </div>

        </div>

    </div>

@endsection