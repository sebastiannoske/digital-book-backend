@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="bread-crump">

            Schulbuch

        </div>

        <div class="row">

            <div class="col-md-12">

                @if (isset($book))

                    @if (session()->has('message'))

                        <p class="alert alert-success">

                            <?php echo session('message')[0]; ?>

                        </p>

                        <br/><br/><br/>

                    @endif


                    <div class="row">

                        <div class="col-md-6">

                            <h4>Cover</h4>

                            <br/>

                            {!! Form::open(['url' => "/books/$book->id/update"]) !!}

                                <div class="form-group  <?php if ($errors->has('title')) echo 'has-error'; ?>">
                                    {{ Form::label('title', 'Titel')}}
                                    {{ Form::text('title', $book->title, array_merge(['class' => 'form-control', 'id' => 'title', 'required' => 'required'])) }}
                                </div>

                                <div class="form-group  <?php if ($errors->has('author')) echo 'has-error'; ?>">
                                    {{ Form::label('author', 'Autor')}}
                                    {{ Form::text('author', $book->author, array_merge(['class' => 'form-control', 'id' => 'author', 'required' => 'required'])) }}
                                </div>

                                <div class="form-group  <?php if ($errors->has('isbn')) echo 'has-error'; ?>">
                                    {{ Form::label('isbn', 'ISBN')}}
                                    {{ Form::text('isbn', $book->isbn, array_merge(['class' => 'form-control', 'id' => 'isbn', 'required' => 'required'])) }}
                                </div>

                                <div class="form-group  <?php if ($errors->has('color')) echo 'has-error'; ?>">
                                    {{ Form::label('color', 'Schriftfarbe')}}
                                    {{ Form::text('color', $book->color, array_merge(['class' => 'form-control', 'id' => 'color', 'required' => 'required'])) }}
                                </div>

                                <div class="right-align">

                                    <button type="submit" class="btn btn-primary">speichern</button>

                                </div>

                            {!! Form::close() !!}

                        </div>

                        <div class="col-md-6">

                            @if (isset($book->image))

                                <div class="preview-image-wrap">

                                    <img src="{{$book->image->image_path}}"/>

                                    <div id="cover-preview-text-warp">

                                        <p style="color:{{$book->color}};">
                                            {{$book->title}}<br/>
                                            von <span>{{$book->author}}</span>
                                        </p>

                                    </div>

                                </div>

                            @else

                                <p>Kein Bild vorhanden</p>

                            @endif

                            <?php echo Form::open(array('route' => array('fileUpload', $book->id), 'enctype' => 'multipart/form-data')); ?>

                            <ul class="list-group">

                                <li class="list-group-item">

                                    <div class="row">

                                        <div class="col-md-8">

                                            <?php echo Form::file('image', array('class' => 'form-control-file')); ?>


                                        </div>

                                        <div class="col-md-4 right-align">

                                            <button type="submit" class="btn btn-primary">Upload</button>

                                        </div>

                                    </div>

                                </li>

                            </ul>

                            <?php echo Form::close(); ?>

                        </div>

                    </div>


                    <br/><br/>

                    @if (isset($book->chapters))

                        <h4>Kapitel</h4>

                        <edit-chapters :data="{{ $book->chapters->toJson() }}"></edit-chapters>

                    @endif

                    <?php echo Form::open(['url' => "/books/$book->id/chapters/store"]); ?>

                        <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Kapitel hinzufügen</button>

                    <?php echo Form::close(); ?>

                    <br/><br/>

                    <div class="row">

                        <div class="col-md-12">

                            <h4>Glossar</h4>

                            @if (isset($book->glossary))

                                <a href="{{ url('/books/' . $book->id . '/glossaries/' . $book->glossary->id) }}" target="_self">
                                    <button type="button" class="btn btn-primary btn-lg btn-block padding-top">Glossar bearbeiten</button>
                                </a>

                            @else

                                <?php echo Form::open(['url' => "/books/$book->id/glossaries/store"]); ?>

                                    <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Glossar hinzufügen</button>

                                 <?php echo Form::close(); ?>

                            @endif

                        </div>

                    </div>

                    <br/><br/>

                    <h4>Inhaltsseiten</h4>

                    <div class="row">

                    @foreach ($book->pageContents as $page_content)

                            <div class="col-md-4">

                                <a href="{{ url('/books/' . $book->id . '/page-contents/' . $page_content->id ) }}" target="_self">
                                    <button type="button" class="btn btn-primary btn-lg btn-block padding-top"><span style="text-transform: capitalize;">{{ $page_content->slug }}</span> bearbeiten</button>
                                </a>

                            </div>

                    @endforeach

                    </div>

                    <br/><br/><br/><br/><br/><br/>

                @else

                    <h3>Sie haben nicht die nötigen Rechte um diese Seite sehen zu können.</h3>

                @endif

            </div>

        </div>

    </div>

@endsection
