@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="bread-crump">

            <a href="{{ url('/books/' . $book_id ) }}" target="_self">Schulbuch</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id) }}" target="_self">Kapitel</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id ) }}" target="_self">Unterkapitel</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id . '/learning-sections/' . $learning_section_id) }}" target="_self">Abschnit / Lernabschnitt</a> >
            Inhaltselement

        </div>

        <div class="row">

            <div class="col-md-12">

                @if (isset($content_element))

                    <?php $formError = $errors->all() ? true : false; ?>

                    @if ($formError)

                        <p class="alert alert-danger">

                            Es ist ein Fehler im Formular beim Anpassen des Kapitels aufgetreten.

                        </p>

                        <br/><br/><br/>

                    @endif

                    @if (session()->has('message'))

                        <p class="alert alert-success">

                            <?php echo session('message')[0]; ?>

                        </p>

                        <br/><br/><br/>

                    @endif

                    @if (!isset($content_element->type))

                        <h3>Inhaltstyp wählen</h3>

                        <div class="row">

                            <div class="col-md-3">

                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/updatetype"]); ?>

                                <input name="type" type="hidden" value="text">

                                <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Text</button>

                                <?php echo Form::close(); ?>

                            </div>

                            <div class="col-md-3">

                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/updatetype"]); ?>

                                <input name="type" type="hidden" value="image">

                                <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Bild</button>

                                <?php echo Form::close(); ?>

                            </div>

                            <div class="col-md-3">

                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/updatetype"]); ?>

                                <input name="type" type="hidden" value="video">

                                <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Video</button>

                                <?php echo Form::close(); ?>

                            </div>

                            <div class="col-md-3">

                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/updatetype"]); ?>

                                <input name="type" type="hidden" value="quote">

                                <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Zitat</button>

                                <?php echo Form::close(); ?>

                            </div>

                            <div class="col-md-3">

                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/updatetype"]); ?>

                                <input name="type" type="hidden" value="infobox">

                                <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Info Box</button>

                                <?php echo Form::close(); ?>

                            </div>

                            <div class="col-md-3">

                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/updatetype"]); ?>

                                <input name="type" type="hidden" value="chart">

                                <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Chart</button>

                                <?php echo Form::close(); ?>

                            </div>

                            <div class="col-md-3">

                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/updatetype"]); ?>

                                <input name="type" type="hidden" value="iframe">

                                <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Iframe</button>

                                <?php echo Form::close(); ?>

                            </div>

                        </div>

                    @else

                        @if ($content_element->type === 'text')

                            <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/update"]); ?>

                            <span class="as-label">ist geschützt? ( Falls nicht, wird dieser Text nicht im herunterladtbaren Dokument enthalten sein )</span>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" id="isProtected" name="isProtected" class="custom-control-input" <?php if ($content_element->is_protected) echo 'checked="checked"'; ?>>
                                <label class="custom-control-label" for="isProtected"><?php if ($content_element->is_protected): echo '( aktuell: ja )'; else: echo '( aktuell: nein )'; endif; ?></label>
                            </div>

                            <br/><br/>

                            <div class="form-group  <?php if ($errors->has('text')) echo 'has-error'; ?>">
                                {{ Form::label('text', 'Text')}}
                                {{ Form::textarea('text', $content_element->text, array_merge(['class' => 'form-control summernote', 'id' => 'text', 'rows' => 7, 'required' => 'required'])) }}
                            </div>

                            <div class="right-align">

                                <button type="submit" class="btn btn-primary">speichern</button>

                            </div>

                            <?php echo Form::close(); ?>

                        @endif

                        @if ($content_element->type === 'infobox')

                            <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/update"]); ?>

                            <div class="form-group  <?php if ($errors->has('text')) echo 'has-error'; ?>">
                                {{ Form::label('text', 'Text')}}
                                {{ Form::textarea('text', $content_element->text, array_merge(['class' => 'form-control summernote', 'id' => 'text', 'rows' => 7, 'required' => 'required'])) }}
                            </div>

                            <div class="right-align">

                                <button type="submit" class="btn btn-primary">speichern</button>

                            </div>

                            <?php echo Form::close(); ?>

                            <div id="info-box">

                                @if ($content_element->textParts)

                                    @foreach ($content_element->textParts as $text_part)

                                        @if ($text_part['type'] === 'math')

                                            $${{$text_part['text']}}$$

                                        @endif

                                    @endforeach

                                    <infobox-image-upload :data="{{ $content_element->toJson() }}"></infobox-image-upload>

                                @endif

                            </div>


                        @endif

                        @if ($content_element->type === 'chart')

                            <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/update"]); ?>

                            <div class="row">

                                <div class="col-md-12">

                                    <span class="as-label">Diagram-Typ</span>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radioBar" name="radioType" class="custom-control-input" value="bar" <?php if ($content_element->chart->type === 'bar') echo 'checked="checked"'; ?>>
                                        <label class="custom-control-label" for="radioBar">Balken</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radioLine" name="radioType" class="custom-control-input" value="line" <?php if ($content_element->chart->type === 'line') echo 'checked="checked"'; ?>>
                                        <label class="custom-control-label" for="radioLine">Linien</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radioPie" name="radioType" class="custom-control-input" value="pie" <?php if ($content_element->chart->type === 'pie') echo 'checked="checked"'; ?>>
                                        <label class="custom-control-label" for="radioPie">Kreis</label>
                                    </div>

                                    <br/><br/>


                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-6">

                                    <span class="as-label">Chart-Titel ( nicht erforderlich )</span>

                                    <div class="form-group  <?php if ($errors->has('title')) echo 'has-error'; ?>">
                                        {{ Form::text('title', $content_element->chart->title, array_merge(['class' => 'form-control', 'id' => 'title'])) }}
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-6">

                                    <span class="as-label">Label Y-Achse ( nicht erforderlich )</span>

                                    <div class="form-group  <?php if ($errors->has('label_y_axis')) echo 'has-error'; ?>">
                                        {{ Form::text('label_y_axis', $content_element->chart->label_y_axis, array_merge(['class' => 'form-control', 'id' => 'label_y_axis'])) }}
                                    </div>

                                    <span class="as-label">Y-Achse Min-Wert ( 0 = default )</span>

                                    <div class="form-group  <?php if ($errors->has('y_axis_min')) echo 'has-error'; ?>">
                                        {{ Form::text('y_axis_min', $content_element->chart->y_axis_min, array_merge(['class' => 'form-control', 'id' => 'y_axis_min', 'required' => 'required'])) }}
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <span class="as-label">Label X-Achse ( nicht erforderlich )</span>

                                    <div class="form-group  <?php if ($errors->has('label_x_axis')) echo 'has-error'; ?>">
                                        {{ Form::text('label_x_axis', $content_element->chart->label_x_axis, array_merge(['class' => 'form-control', 'id' => 'label_x_axis'])) }}
                                    </div>

                                    <span class="as-label">Y-Achse Max-Wert ( 0 = default )</span>

                                    <div class="form-group  <?php if ($errors->has('y_axis_max')) echo 'has-error'; ?>">
                                        {{ Form::text('y_axis_max', $content_element->chart->y_axis_max, array_merge(['class' => 'form-control', 'id' => 'y_axis_max', 'required' => 'required'])) }}
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <span class="as-label">Beschreibung ( nicht erforderlich )</span>

                                    <div class="form-group  <?php if ($errors->has('description')) echo 'has-error'; ?>">
                                        {{ Form::textarea('description', $content_element->chart->description, array_merge(['class' => 'form-control', 'id' => 'description', 'rows' => 4])) }}
                                    </div>

                                </div>

                            </div>

                            <div class="right-align" style="border-top: 1px solid #ced4da;margin-top: 15px;padding-top: 15px;">

                                <button type="submit" class="btn btn-primary">speichern</button>

                            </div>

                            <?php echo Form::close(); ?>

                            <br/><br/>

                            <h4>Chart Label</h4>

                            @if ($content_element->chart->chartLabels)

                                @foreach ($content_element->chart->chartLabels as $chart_label)

                                <div class="row chapter-row">

                                    <div class="col-md-11">

                                        <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/charts/{$content_element->chart->id}/chart-labels/{$chart_label->id}/update"]); ?>

                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group  <?php if ($errors->has('label')) echo 'has-error'; ?>">
                                                    {{ Form::text('label', $chart_label->label, array_merge(['class' => 'form-control', 'id' => 'label', 'required' => 'required'])) }}
                                                </div>

                                            </div>

                                            <div class="col-md-6 right-align" style="padding-right:5px;">

                                                <button type="submit" class="btn btn-primary">Speichern</button>

                                            </div>

                                        </div>

                                        <?php echo Form::close(); ?>

                                    </div>

                                    <div class="col-md-1 right-align" style="padding-left:0;">

                                        <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/charts/{$content_element->chart->id}/chart-labels/{$chart_label->id}/delete"]); ?>

                                        <button type="submit" class="btn btn-danger">löschen</button>

                                        <?php echo Form::close(); ?>

                                    </div>

                                </div>

                                @endforeach

                            @endif

                            @if (sizeof($content_element->chart->chartLabels) < 25)

                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/charts/{$content_element->chart->id}/storelabel"]); ?>

                                <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Label hinzufügen</button>

                                <?php echo Form::close(); ?>

                            @endif

                            <br/><br/>

                            <h4>Chart-Elemente</h4>

                            <?php $dataset_index = 1; ?>

                            @foreach ($content_element->chart->chartDatasets as $chart_dataset)

                                <br/><br/>

                                <div class="chart-dataset-wrap">

                                    <h5>Chart-Datensatz {{$dataset_index}}</h5>

                                    <?php $dataset_index++; ?>

                                    <span class="as-label">Label</span>

                                    <div class="row">

                                        <div class="col-md-11">

                                            <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/charts/{$content_element->chart->id}/datasets/{$chart_dataset->id}/update"]); ?>

                                            <div class="row">

                                                <div class="col-md-6">

                                                    <div class="form-group  <?php if ($errors->has('label')) echo 'has-error'; ?>">
                                                        {{ Form::text('label', $chart_dataset->label, array_merge(['class' => 'form-control', 'id' => 'label', 'required' => 'required'])) }}
                                                    </div>

                                                </div>

                                                <div class="col-md-6 right-align" style="padding-right:5px;">

                                                    <button type="submit" class="btn btn-primary">Speichern</button>

                                                </div>

                                            </div>

                                            <?php echo Form::close(); ?>

                                        </div>

                                        <div class="col-md-1 right-align" style="padding-left:0;">

                                            <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/charts/{$content_element->chart->id}/datasets/{$chart_dataset->id}/delete"]); ?>

                                            <button type="submit" class="btn btn-danger">löschen</button>

                                            <?php echo Form::close(); ?>

                                        </div>

                                    </div>


                                    <div class="row chapter-row">

                                        <div class="col-md-11">

                                            <div class="row">

                                                <div class="col-md-3">

                                                    <span class="as-label">Wert</span>

                                                </div>

                                                <div class="col-md-3">

                                                    <span class="as-label">Farbe</span>

                                                </div>

                                                <div class="col-md-3">

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    @foreach ($chart_dataset->chartItems as $chart_item)

                                        <div class="row chapter-row">

                                            <div class="col-md-11">

                                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/charts/{$content_element->chart->id}/datasets/{$chart_dataset->id}/chart-items/{$chart_item->id}/update"]); ?>

                                                <div class="row">

                                                    <div class="col-md-3">

                                                        <div class="form-group  <?php if ($errors->has('source')) echo 'has-error'; ?>">
                                                            {{ Form::text('value', $chart_item->value, array_merge(['class' => 'form-control', 'id' => 'value', 'required' => 'required'])) }}
                                                        </div>

                                                    </div>

                                                    <div class="col-md-3">

                                                        <div class="form-group  <?php if ($errors->has('source')) echo 'has-error'; ?>">
                                                            {{ Form::text('color', $chart_item->color, array_merge(['class' => 'form-control', 'id' => 'color', 'required' => 'required'])) }}
                                                        </div>

                                                    </div>

                                                    <div class="col-md-6 right-align" style="padding-right:5px;">

                                                        <button type="submit" class="btn btn-primary">Speichern</button>

                                                    </div>

                                                </div>

                                                <?php echo Form::close(); ?>

                                            </div>

                                            <div class="col-md-1 right-align" style="padding-left:0;">

                                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/charts/{$content_element->chart->id}/datasets/{$chart_dataset->id}web/chart-items/{$chart_item->id}/delete"]); ?>

                                                <button type="submit" class="btn btn-danger">löschen</button>

                                                <?php echo Form::close(); ?>

                                            </div>

                                        </div>

                                    @endforeach


                                    @if (sizeof($chart_dataset->chartItems) < sizeof($content_element->chart->chartLabels))

                                        <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/charts/{$content_element->chart->id}/datasets/$chart_dataset->id/store"]); ?>

                                        <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Chartelement hinzufügen</button>

                                        <?php echo Form::close(); ?>

                                    @endif

                                </div>

                            @endforeach

                            @if (sizeof($content_element->chart->chartLabels) > 0 && sizeof($content_element->chart->chartDatasets) < 5)

                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/charts/{$content_element->chart->id}/store"]); ?>

                                <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Datenset hinzufügen</button>

                                <?php echo Form::close(); ?>

                            @endif


                            <chart-image-upload :data="{{ $content_element->chart->toJson() }}"></chart-image-upload>


                        @endif

                        @if ($content_element->type === 'video')

                            <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/update"]); ?>

                            <div class="form-group  <?php if ($errors->has('source')) echo 'has-error'; ?>">
                                {{ Form::label('source', 'Video URL')}}
                                {{ Form::text('source', $content_element->source, array_merge(['class' => 'form-control', 'id' => 'source', 'required' => 'required'])) }}
                            </div>

                            <div class="form-group  <?php if ($errors->has('description')) echo 'has-error'; ?>">
                                {{ Form::label('description', 'Beschreibung')}}
                                {{ Form::text('description', $content_element->description, array_merge(['class' => 'form-control', 'id' => 'description', 'required' => 'required'])) }}
                            </div>

                            <div class="right-align">

                                <button type="submit" class="btn btn-primary">speichern</button>

                            </div>

                            <?php echo Form::close(); ?>

                        @endif

                        @if ($content_element->type === 'iframe')

                            <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/update"]); ?>

                            <div class="form-group  <?php if ($errors->has('source')) echo 'has-error'; ?>">
                                {{ Form::label('source', 'IFrame URL')}}
                                {{ Form::text('source', $content_element->source, array_merge(['class' => 'form-control', 'id' => 'source', 'required' => 'required'])) }}
                            </div>

                            <div class="form-group  <?php if ($errors->has('description')) echo 'has-error'; ?>">
                                {{ Form::label('description', 'Beschreibung')}}
                                {{ Form::text('description', $content_element->description, array_merge(['class' => 'form-control', 'id' => 'description', 'required' => 'required'])) }}
                            </div>

                            <div class="right-align">

                                <button type="submit" class="btn btn-primary">speichern</button>

                            </div>

                            <?php echo Form::close(); ?>

                        @endif

                        @if ($content_element->type === 'image')

                            @if (isset($content_element->image))

                                <div class="preview-image-wrap">

                                    <img src="{{$content_element->image->image_path}}"/>

                                </div>

                            @else

                                <p>Kein Bild vorhanden</p>

                            @endif

                            <?php echo Form::open(array('route' => array('contentFileUpload', $content_element->id), 'enctype' => 'multipart/form-data')); ?>

                            <ul class="list-group">

                                <li class="list-group-item">

                                    <div class="row">

                                        <div class="col-md-8">

                                            <?php echo Form::file('image', array('class' => 'form-control-file')); ?>


                                        </div>

                                        <div class="col-md-4 right-align">

                                            <button type="submit" class="btn btn-primary">Upload</button>

                                        </div>

                                    </div>

                                </li>

                            </ul>

                            <?php echo Form::close(); ?>

                            @if (isset($content_element->image) && isset($content_element->image))

                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/updateImageDescription"]); ?>

                                <br/><br/>

                                <div class="row">

                                    <div class="col-md-4">

                                        <span class="as-label">Bild Anzeige-Größe</span>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radioLarge" name="radioSize" class="custom-control-input" value="large" <?php if ($content_element->image->size === 'large') echo 'checked="checked"'; ?>>
                                            <label class="custom-control-label" for="radioLarge">Groß</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radioMedium" name="radioSize" class="custom-control-input" value="medium" <?php if ($content_element->image->size === 'medium') echo 'checked="checked"'; ?>>
                                            <label class="custom-control-label" for="radioMedium">Mittel</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radioSmall" name="radioSize" class="custom-control-input" value="small" <?php if ($content_element->image->size === 'small') echo 'checked="checked"'; ?>>
                                            <label class="custom-control-label" for="radioSmall">Klein</label>
                                        </div>

                                    </div>

                                    <div class="col-md-8">

                                        <div class="form-group  <?php if ($errors->has('description')) echo 'has-error'; ?>">
                                            {{ Form::label('description', 'Beschreibung')}}
                                            {{ Form::text('description', $content_element->image->description, array_merge(['class' => 'form-control', 'id' => 'description'])) }}
                                        </div>

                                    </div>

                                </div>

                                <div class="right-align">

                                    <button type="submit" class="btn btn-primary">speichern</button>

                                </div>

                                <?php echo Form::close(); ?>

                            @endif

                        @endif

                        @if ($content_element->type === 'quote')

                            @if (isset($content_element->imagesource) && strlen($content_element->imagesource))

                                <div class="preview-image-wrap">

                                    <img src="{{$content_element->imagesource}}"/>

                                </div>

                            @else

                                <p>Kein Bild vorhanden</p>

                            @endif

                            <?php echo Form::open(array('route' => array('contentFileUpload', $content_element->id), 'enctype' => 'multipart/form-data')); ?>

                            <ul class="list-group">

                                <li class="list-group-item">

                                    <div class="row">

                                        <div class="col-md-8">

                                            <?php echo Form::file('image', array('class' => 'form-control-file')); ?>


                                        </div>

                                        <div class="col-md-4 right-align">

                                            <button type="submit" class="btn btn-primary">Upload</button>

                                        </div>

                                    </div>

                                </li>

                            </ul>

                            <?php echo Form::close(); ?>

                            <br/>

                            <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section_id/content-element/$content_element->id/update"]); ?>

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group  <?php if ($errors->has('text')) echo 'has-error'; ?>">
                                        {{ Form::label('text', 'Text')}}
                                        {{ Form::textarea('text', $content_element->text, array_merge(['class' => 'form-control', 'id' => 'text', 'rows' => 4, 'required' => 'required'])) }}
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-9">

                                    <div class="form-group  <?php if ($errors->has('originator')) echo 'has-error'; ?>">
                                        {{ Form::label('originator', 'Urheber')}}
                                        {{ Form::text('originator', $content_element->originator, array_merge(['class' => 'form-control', 'id' => 'originator', 'required' => 'required'])) }}
                                    </div>

                                </div>

                                <div class="col-md-3">

                                    <div class="form-group  <?php if ($errors->has('year')) echo 'has-error'; ?>">
                                        {{ Form::label('year', 'Jahr')}}
                                        {{ Form::text('year', $content_element->year, array_merge(['class' => 'form-control', 'id' => 'year', 'required' => 'required'])) }}
                                    </div>

                                </div>

                            </div>


                            <div class="right-align">

                                <button type="submit" class="btn btn-primary">speichern</button>

                            </div>

                            <?php echo Form::close(); ?>

                        @endif

                    @endif

                @else

                    <h3>Sie haben nicht die nötigen Rechte um diese Seite sehen zu können.</h3>

                @endif

            </div>

        </div>

    </div>

@endsection