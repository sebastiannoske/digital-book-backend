@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="bread-crump">

            <a href="{{ url('/books/' . $book_id ) }}" target="_self">Schulbuch</a> >
            Inhaltsseite

        </div>

        <div class="row">

            <div class="col-md-12">

                @if (isset($page_content))

                    <?php $formError = $errors->all() ? true : false; ?>

                    @if ($formError)

                        <p class="alert alert-danger">

                            Es ist ein Fehler im Formular beim Anpassen des Kapitels aufgetreten.

                        </p>

                        <br/><br/><br/>

                    @endif

                    @if (session()->has('message'))

                        <p class="alert alert-success">

                            <?php echo session('message')[0]; ?>

                        </p>

                        <br/><br/><br/>

                    @endif

                    <h1 style="text-transform: capitalize;">{{$page_content->slug}}</h1>

                    {!! Form::open(['url' => "/books/$book_id/page-contents/$page_content->id/update"]) !!}

                    <div class="form-group <?php if ($errors->has('text')) echo 'has-error'; ?>">
                        {{ Form::label('text', 'Inhalt')}}
                        {{ Form::textarea('text', $page_content->text, array_merge(['class' => 'form-control summernote', 'id' => 'text', 'rows' => 30, 'required' => 'required'])) }}
                    </div>

                    <div class="right-align">

                        <button type="submit" class="btn btn-primary">speichern</button>

                    </div>

                    {!! Form::close() !!}

                @endif

            </div>

        </div>

    </div>

@endsection
