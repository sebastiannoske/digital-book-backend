@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="bread-crump">

            <a href="{{ url('/books/' . $book_id) }}" target="_self">Schulbuch</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id) }}" target="_self">Kapitel</a>
            > Unterkapitel

        </div>

        <div class="row">

            <div class="col-md-12">

                @if (isset($sub_chapter))

                    <?php $formError = $errors->all() ? true : false; ?>

                    @if ($formError)

                        <p class="alert alert-danger">

                            Es ist ein Fehler im Formular beim Anpassen des Kapitels aufgetreten.

                        </p>

                        <br/><br/><br/>

                    @endif

                    @if (session()->has('message'))

                        <p class="alert alert-success">

                            <?php echo session('message')[0]; ?>

                        </p>

                        <br/><br/><br/>

                    @endif

                    {!! Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter->id/update"]) !!}

                    @if ($sub_chapter->is_sub_chapter)

                        <div class="form-group  <?php if ($errors->has('title')) echo 'has-error'; ?>">
                            {{ Form::label('title', 'Titel')}}
                            {{ Form::text('title', $sub_chapter->title, array_merge(['class' => 'form-control', 'id' => 'title'])) }}
                        </div>

                    @endif

                    <div class="row">

                        <div class="col-md-12">

                            <span class="as-label">Überschrift anzeigen? ( Falls nicht, wird diese Sektion nicht als Unterkapitel angezeigt )</span>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" id="isSubChapter" name="isSubChapter" class="custom-control-input" <?php if ($sub_chapter->is_sub_chapter) echo 'checked="checked"'; ?>>
                                <label class="custom-control-label" for="isSubChapter"><?php if ($sub_chapter->is_sub_chapter): echo '( aktuell: ja )'; else: echo '( aktuell: ja )'; endif; ?></label>
                            </div>

                        </div>

                    </div>


                    <div class="right-align">

                        <button type="submit" class="btn btn-primary">speichern</button>

                    </div>

                    {!! Form::close() !!}

                <!-- 'color', 'imagesource' -->

                    @if (isset($sub_chapter->learningSections))

                        <h4>Abschnitte / Lernabschnitte</h4>

                        <edit-learning-sections :data="{{ $sub_chapter->learningSections->toJson() }}"></edit-learning-sections>

                    @endif

                    @if (strlen($sub_chapter->title) || !$sub_chapter->is_sub_chapter)

                        <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter->id/learning-sections/store"]); ?>

                        <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Abschnitt / Lernabschnitt hinzufügen</button>

                        <?php echo Form::close(); ?>

                    @endif

                @else

                    <h3>Sie haben nicht die nötigen Rechte um diese Seite sehen zu können.</h3>

                @endif

            </div>

        </div>

    </div>

@endsection
