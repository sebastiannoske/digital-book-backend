@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="bread-crump">

            <a href="{{ url('/books/' . $book_id ) }}" target="_self">Schulbuch</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id) }}" target="_self">Kapitel</a> >
            <a href="{{ url('/books/' . $book_id  . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id ) }}" target="_self">Unterkapitel</a> >
            Lernabschnitt

        </div>

        <div class="row">

            <div class="col-md-12">

                @if (isset($learning_section))

                    <?php $formError = $errors->all() ? true : false; ?>

                    @if ($formError)

                        <p class="alert alert-danger">

                            Es ist ein Fehler im Formular beim Anpassen des Kapitels aufgetreten.

                        </p>

                        <br/><br/><br/>

                    @endif

                    @if (session()->has('message'))

                        <p class="alert alert-success">

                            <?php echo session('message')[0]; ?>

                        </p>

                        <br/><br/><br/>

                    @endif

                    {!! Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section->id/update"]) !!}

                    @if ($learning_section->is_learning_section)

                         <div class="form-group  <?php if ($errors->has('title')) echo 'has-error'; ?>">
                            {{ Form::label('title', 'Titel')}}
                            {{ Form::text('title', $learning_section->title, array_merge(['class' => 'form-control', 'id' => 'title'])) }}
                        </div>

                        <div class="form-group <?php if ($errors->has('introduction')) echo 'has-error'; ?>">
                            {{ Form::label('introduction', 'Einleitung')}}
                            {{ Form::textarea('introduction', $learning_section->introduction, array_merge(['class' => 'form-control', 'id' => 'introduction', 'rows' => 7])) }}
                        </div>

                    @endif

                    <div class="row">

                        <div class="col-md-4">

                            <span class="as-label">Glossar für aktuellen Abschnitt aktivieren?</span>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" id="keywordsEnabled" name="keywordsEnabled" class="custom-control-input" <?php if ($learning_section->keywords_enabled) echo 'checked="checked"'; ?>>
                                <label class="custom-control-label" for="keywordsEnabled"><?php if ($learning_section->keywords_enabled): echo '( aktuell: ja )'; else: echo '( aktuell: nein )'; endif; ?></label>
                            </div>

                        </div>

                        <div class="col-md-4">

                            <span class="as-label">Ist ein Lernabschnitt?</span>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" id="isLearningSection" name="isLearningSection" class="custom-control-input" <?php if ($learning_section->is_learning_section) echo 'checked="checked"'; ?>>
                                <label class="custom-control-label" for="isLearningSection"><?php if ($learning_section->is_learning_section): echo '( aktuell: ja )'; else: echo '( aktuell: nein )'; endif; ?></label>
                            </div>

                        </div>

                    </div>

                    <div class="right-align">

                        <button type="submit" class="btn btn-primary">speichern</button>

                    </div>

                    {!! Form::close() !!}

                <!-- 'color', 'imagesource' -->

                    @if (isset($learning_section->content))

                        <h4>Inhaltselemente</h4>

                        <edit-content-elements :data="{{ $learning_section->toJson() }}"></edit-content-elements>

                    @endif

                    @if (!$learning_section->is_learning_section || (strlen($learning_section->title) && strlen($learning_section->introduction)))

                        <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section->id/content-element/store"]); ?>

                            <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Inhaltselement hinzufügen</button>

                        <?php echo Form::close(); ?>

                    @endif

                    @if ($learning_section->is_learning_section && strlen($learning_section->title) && strlen($learning_section->introduction))

                        @if (isset($learning_section->mediaLinks))

                            <br/><br/>

                            <h4>Medien und Links</h4>

                            <ul class="list-group padding">

                                @foreach ($learning_section->mediaLinks as $media_link)

                                    <li class="list-group-item disabled">

                                        <div class="row chapter-row">

                                            <div class="col-md-8">

                                                <h5>{{$media_link->display_name}}</h5>

                                            </div>

                                            <div class="col-md-4 right-align">

                                                <a href="{{ url('/books/' . $book_id . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id .'/learning-sections/' . $learning_section->id . '/media-links/' . $media_link->id) }}" target="_self"><button type="button" class="btn btn-primary">Bearbeiten</button></a>

                                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section->id/media-links/$media_link->id/delete"]); ?>

                                                <button type="submit" class="btn btn-danger">löschen</button>

                                                <?php echo Form::close(); ?>

                                            </div>

                                        </div>

                                    </li>

                                @endforeach

                            </ul>

                        @endif

                        <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section->id/media-links/store"]); ?>

                        <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Medium oder Link hinzufügen</button>

                        <?php echo Form::close(); ?>

                    @endif

                    @if (isset($learning_section->furtherInformation))

                        <br/><br/>

                        <h4>Zusatzinformation/Lizenzangaben</h4>

                        <ul class="list-group padding">

                            @foreach ($learning_section->furtherInformation as $further_information)

                                <li class="list-group-item disabled">

                                    <div class="row chapter-row">

                                        <div class="col-md-8">

                                            <h5>{{$further_information->display_name}}</h5>

                                        </div>

                                        <div class="col-md-4 right-align">

                                            <a href="{{ url('/books/' . $book_id . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id .'/learning-sections/' . $learning_section->id . '/further-information/' . $further_information->id) }}" target="_self"><button type="button" class="btn btn-primary">Bearbeiten</button></a>

                                            <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section->id/further-information/$further_information->id/delete"]); ?>

                                            <button type="submit" class="btn btn-danger">löschen</button>

                                            <?php echo Form::close(); ?>

                                        </div>

                                    </div>

                                </li>

                            @endforeach

                        </ul>

                    @endif

                    <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section->id/further-information/store"]); ?>

                    <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Zusatzinformation/Lizenzangaben hinzufügen</button>

                    <?php echo Form::close(); ?>


                    @if ($learning_section->is_learning_section && strlen($learning_section->title) && strlen($learning_section->introduction))

                        @if (isset($learning_section->workOrders))

                            <br/><br/>

                            <h4>Arbeitsaufträge</h4>

                            <ul class="list-group padding">

                                @foreach ($learning_section->workOrders as $work_order)

                                    <li class="list-group-item disabled">

                                        <div class="row chapter-row">

                                            <div class="col-md-8">

                                                <h5>{{$work_order->text}}</h5>

                                            </div>

                                            <div class="col-md-4 right-align">

                                                <a href="{{ url('/books/' . $book_id . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id .'/learning-sections/' . $learning_section->id . '/work-orders/' . $work_order->id) }}" target="_self"><button type="button" class="btn btn-primary">Bearbeiten</button></a>

                                                <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section->id/work-orders/$work_order->id/delete"]); ?>

                                                <button type="submit" class="btn btn-danger">löschen</button>

                                                <?php echo Form::close(); ?>

                                            </div>

                                        </div>

                                    </li>

                                @endforeach

                            </ul>

                        @endif

                        <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter_id/sub-chapters/$sub_chapter_id/learning-sections/$learning_section->id/work-orders/store"]); ?>

                        <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Arbeitsauftrag hinzufügen</button>

                        <?php echo Form::close(); ?>

                    @endif

                @else

                    <h3>Sie haben nicht die nötigen Rechte um diese Seite sehen zu können.</h3>

                @endif

            </div>

        </div>

    </div>

@endsection
