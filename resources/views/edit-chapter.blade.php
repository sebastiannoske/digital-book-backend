@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="bread-crump">

            <a href="{{ url('/books/' . $book_id) }}" target="_self">Schulbuch</a> > Kapitel

        </div>

        <div class="row">

            <div class="col-md-12">

                @if (isset($chapter))

                    <?php $formError = $errors->all() ? true : false; ?>

                    @if ($formError)

                        <p class="alert alert-danger">

                            Es ist ein Fehler im Formular beim Anpassen des Kapitels aufgetreten.

                        </p>

                        <br/><br/><br/>

                    @endif

                    @if (session()->has('message'))

                        <p class="alert alert-success">

                            <?php echo session('message')[0]; ?>

                        </p>

                        <br/><br/><br/>

                    @endif

                    {!! Form::open(['url' => "/books/$book_id/chapters/$chapter->id/update"]) !!}

                        <div class="form-group  <?php if ($errors->has('title')) echo 'has-error'; ?>">
                            {{ Form::label('title', 'Titel')}}
                            {{ Form::text('title', $chapter->title, array_merge(['class' => 'form-control', 'id' => 'title', 'required' => 'required'])) }}
                        </div>

                        <div class="form-group <?php if ($errors->has('introduction')) echo 'has-error'; ?>">
                            {{ Form::label('introduction', 'Einleitung')}}
                            {{ Form::textarea('introduction', $chapter->introduction, array_merge(['class' => 'form-control', 'id' => 'introduction', 'rows' => 7, 'required' => 'required'])) }}
                        </div>

                        <div class="right-align">

                            <button type="submit" class="btn btn-primary">speichern</button>

                        </div>

                    {!! Form::close() !!}

                    <!-- 'color', 'imagesource' -->

                    @if (isset($chapter->subChapters))

                        <h4>Unterkapitel</h4>

                        <edit-subchapters :data="{{ $chapter->subChapters->toJson() }}"></edit-subchapters>

                    @endif

                    @if (strlen($chapter->title) && strlen($chapter->introduction))

                        <?php echo Form::open(['url' => "/books/$book_id/chapters/$chapter->id/sub-chapters/store"]); ?>

                            <button type="submit" class="btn btn-primary btn-lg btn-block padding-top">Unterkapitel hinzufügen</button>

                        <?php echo Form::close(); ?>

                    @endif

                @else

                    <h3>Sie haben nicht die nötigen Rechte um diese Seite sehen zu können.</h3>

                @endif

            </div>

        </div>

    </div>

@endsection
