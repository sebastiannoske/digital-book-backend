var Global = Global || {};


(function($){


    Global.Controller = (function(self) {

        var elements = {};
        var myChart = null;

        var construct = function construct() {

            $('.summernote').summernote(
                {
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['link'],
                        ['table'],
                        ['codeview']
                    ], height: 400
                }
            );

            var evt = new CustomEvent("domeIsReady");
            window.dispatchEvent(evt);


            MathJax.Hub.Register.StartupHook("End",function () {
                var selector = document.querySelector("#MathJax-Span-1");
                if (selector) {
                    html2canvas(selector, { background :'#FFFFFF' }).then(canvas => {
                        document.getElementById('math-wrap').appendChild(canvas);
                        $('#MathJax-Span-1').remove();
                    });
                }
                /*
                var svg = document.querySelector('#info-box svg');
                var serializer = new XMLSerializer();
                var svgString = serializer.serializeToString(svg);
                var encodedData = window.btoa(svgString);
                var img = document.getElementById('myimg');
                img.src = 'data:image/svg+xml;base64,' + encodedData;

                var canvas = document.getElementById("canvas");*/
                // canvg(canvas, );
            });
        };

        return {
            init: construct,
            elements: elements
        };

    })(Global.Controller || {});

    $(document).ready(Global.Controller.init);


})(jQuery);
