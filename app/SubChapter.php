<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubChapter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['title', 'is_sub_chapter', 'chapter_id', 'sort_order'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['created_at', 'updated_at', 'chapter_id'];

    /**
     *  Get chapter associated with the learning section
     */

    public function chapter() {

        return $this->belongsTo('App\Chapter');

    }

    /**
     *  Get learnin sections associated with the chapter
     */

    public function learningSections() {

        return $this->hasMany('App\LearningSection')->orderBy('sort_order', 'asc');

    }
}
