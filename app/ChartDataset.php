<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChartDataset extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['label', 'chart_id'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['created_at', 'updated_at', 'chart_id'];

    /**
     *  Get chart associated with the chart item
     */

    public function chart() {

        return $this->belongsTo('App\Chart');

    }

    /**
     *  Get chart items associated with the chart
     */

    public function chartItems() {

        return $this->hasMany('App\ChartItem');

    }
}
