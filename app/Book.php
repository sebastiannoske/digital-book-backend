<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['title', 'author', 'isbn', 'color', 'image_id', 'sort_order'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['created_at', 'updated_at'];

    /**
     *  Get chapters associated with the book
     */

    public function chapters() {

        return $this->hasMany('App\Chapter')->orderBy('sort_order', 'asc');

    }

    /**
     *  Get image associated with the book
     */

    public function image() {

        return $this->belongsTo('App\Image');

    }

    /**
     *  Get glossary associated with the book
     */

    public function glossary() {

        return $this->hasOne('App\Glossary');

    }

    /**
     *  Get glossary associated with the book
     */

    public function pageContents() {

        return $this->hasMany('App\PageContent');

    }
}
