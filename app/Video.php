<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['source', 'description', 'content_element_id'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['created_at', 'updated_at', 'content_element_id'];

    /**
     *  Get contentElement associated with the video
     */

    public function contentElement() {

        return $this->belongsTo('App\ContentElement');

    }
}
