<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'description',
        'image_original_path',
        'image_path',
        'image_thumb_path',
        'width_original',
        'width',
        'width_thumb',
        'height_original',
        'height',
        'height_thumb',
        'file_size_original',
        'file_size',
        'file_size_thumb',
        'size',
        'content_element_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'created_at', 'updated_at', 'content_element_id'];

    /**
     *  Get contentElement associated with the image
     */

    public function contentElement() {

        return $this->belongsTo('App\ContentElement');

    }
}
