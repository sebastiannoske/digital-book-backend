<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['type', 'title', 'description', 'y_axis_min', 'y_axis_max', 'label_x_axis', 'label_y_axis', 'content_element_id'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['created_at', 'updated_at', 'content_element_id'];

    /**
     *  Get contentElement associated with the text
     */

    public function contentElement() {

        return $this->belongsTo('App\ContentElement');

    }

    /**
     *  Get chart datasets associated with the chart
     */

    public function chartDatasets() {

        return $this->hasMany('App\ChartDataset');

    }

    /**
     *  Get chart labels associated with the chart
     */

    public function chartLabels() {

        return $this->hasMany('App\ChartLabel');

    }


}
