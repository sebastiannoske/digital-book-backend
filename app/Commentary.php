<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commentary extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['glossary_id', 'glosse', 'reference', 'comment'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['book_id', 'created_at', 'updated_at', 'id'];

    /**
     *  Get book associated with the glassary
     */

    public function glassary() {

        return $this->belongsTo('App\Glossary');

    }
}
