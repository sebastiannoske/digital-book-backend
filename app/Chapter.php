<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['title', 'introduction', 'book_id', 'sort_order'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['book_id', 'created_at', 'updated_at'];

    /**
     *  Get book associated with the chapter
     */

    public function book() {

        return $this->belongsTo('App\Book');

    }

    /**
     *  Get learnin sections associated with the chapter
     */

    public function subChapters() {

        return $this->hasMany('App\SubChapter')->orderBy('sort_order', 'asc');

    }
}
