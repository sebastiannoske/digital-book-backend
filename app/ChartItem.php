<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChartItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['value', 'color', 'dataset_id'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['created_at', 'updated_at', 'id', 'chart_id'];

    /**
     *  Get chart associated with the chart item
     */

    public function chartDataset() {

        return $this->belongsTo('App\ChartDataset');

    }
}
