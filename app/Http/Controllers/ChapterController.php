<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Chapter;
use App\SubChapter;

class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $book_id)
    {
        $chapter = Chapter::create([
            'book_id' => $book_id
        ]);



        return redirect('books/' . $book_id . '/chapters/' . $chapter->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSubChapter(Request $request, $book_id, $chapter_id)
    {
        $sub_chapter = SubChapter::create([
            'chapter_id' => $chapter_id
        ]);



        return redirect('books/' . $book_id . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($book_id, $chapter_id)
    {
        $auth_user = \Auth::user();
        $chapter = null;

        if ($auth_user) {
            $chapter = Chapter::where('id', '=', $chapter_id)->with('subChapters')->first();
        }

        return view('edit-chapter', ['book_id' => $book_id, 'chapter' => $chapter]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editSubChapter($book_id, $chapter_id, $sub_chapter_id)
    {
        $auth_user = \Auth::user();
        $sub_chapter = null;

        if ($auth_user) {
            $sub_chapter = SubChapter::where('id', '=', $sub_chapter_id)->with('learningSections')->first();
        }

        return view('subchapter.edit', ['book_id' => $book_id, 'chapter_id' => $chapter_id, 'sub_chapter' => $sub_chapter]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateChapterRequest $request, $book_id, $chapter_id)
    {
        $chapter = Chapter::find($chapter_id);
        $chapter->title = $request->title;
        $chapter->introduction = $request->introduction;
        $chapter->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }

    /**
     * Update order of content elements
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSubChapterOrder(Request $request, $book_id, $chapter_id)
    {
        $auth_user = \Auth::user();
        $chapter = null;

        if ($auth_user) {
            $chapter = Chapter::where('id', '=', $chapter_id)->with('subChapters')->first();
        }



        $chapter->subChapters->each(function($subChapters, $index) use ($request) {
            $myIndex = 0;
            $tempIndex = 0;

            foreach($request->elements as $key => $value) {
                if ($value['id'] === $subChapters->id) {
                    $myIndex = $tempIndex;
                }
                $tempIndex++;
            };
            $subChapters->update(array_only($request->elements[$myIndex], ['sort_order']));
        });
    }

    /**
     * Update order of content elements
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateLearningSectionOrder(Request $request, $book_id, $chapter_id, $sub_chapter_id)
    {
        $auth_user = \Auth::user();
        $sub_chapter = null;

        if ($auth_user) {
            $sub_chapter = SubChapter::where('id', '=', $sub_chapter_id)->with('learningSections')->first();
        }



        $sub_chapter->learningSections->each(function($learningSection, $index) use ($request) {
            $myIndex = 0;
            $tempIndex = 0;

            foreach($request->elements as $key => $value) {
                if ($value['id'] === $learningSection->id) {
                    $myIndex = $tempIndex;
                }
                $tempIndex++;
            };
            $learningSection->update(array_only($request->elements[$myIndex], ['sort_order']));
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSubChapter(Requests\UpdateSubChapterRequest $request, $book_id, $chapter_id, $sub_chapter_id)
    {
        $sub_chapter = SubChapter::find($sub_chapter_id);
        $sub_chapter->is_sub_chapter = $request->isSubChapter === 'on';
        if ($sub_chapter->is_sub_chapter) {
            $sub_chapter->title = $request->title;
        } else {
            $sub_chapter->title = '';
        }
        $sub_chapter->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($book_id, $chapter_id)
    {
        $chapter = Chapter::findOrFail($chapter_id);

        if ($chapter) {
            $chapter->delete();
        }

        return redirect()->back()->with('message', ['Das Bild wurde erfolgreich gespeichert.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroySubChapter($book_id, $chapter_id, $sub_chapter_id)
    {
        $sub_chapter = SubChapter::findOrFail($sub_chapter_id);

        if ($sub_chapter) {
            $sub_chapter->delete();
        }

        return redirect()->back()->with('message', ['Das Bild wurde erfolgreich gespeichert.']);
    }
}
