<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use App\ContentElement;
use App\Text;
use App\InfoBox;
use App\Video;
use App\Iframe;
use App\Image;
use App\Quote;
use App\Chart;
use App\ChartDataset;
use App\ChartLabel;
use App\ChartItem;

class ContentElementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id)
    {
        $currentElements = ContentElement::where('learning_section_id', '=', $learning_section_id)->get();

        $orderValue = 0;
        $currentElements->each(function($content, $index) use (&$orderValue) {
            $orderValue = max($orderValue, $content->sort_order);
        });
        $orderValue++;

        $content_element = ContentElement::create([
            'learning_section_id' => $learning_section_id,
            'sort_order' => $orderValue
        ]);

        return redirect('books/' . $book_id . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id . '/learning-sections/' . $learning_section_id . '/content-element/'. $content_element->id);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeChartLabel(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id, $chart_id)
    {
        ChartLabel::create([
            'chart_id' => $chart_id
        ]);

        return redirect()->back()->with('message', ['Chart-Label erfolgreich angelegt.']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeChartDataset(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id, $chart_id)
    {
        ChartDataset::create([
            'chart_id' => $chart_id
        ]);

        return redirect()->back()->with('message', ['Chart-Dataset erfolgreich angelegt.']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteDataset(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id, $chart_id, $dataset_id)
    {
        $chart_dataset = ChartDataset::findOrFail($dataset_id);

        if ($chart_dataset) {
            $chart_dataset->delete();
        }

        return redirect()->back()->with('message', ['Der Chart-Datensatz wurde gelöscht.']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateDatasetLabel(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id, $chart_id, $dataset_id)
    {
        $chart_dataset = ChartDataset::find($dataset_id);
        $chart_dataset->label = $request->label;
        $chart_dataset->save();

        return redirect()->back()->with('message', ['Der Chart-Datensatz wurde erfolgreich aktualisiert.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeChartItem(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id, $chart_id, $dataset_id)
    {
        ChartItem::create([
            'dataset_id' => $dataset_id
        ]);

        return redirect()->back()->with('message', ['Chart-Element erfolgreich angelegt.']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteChartItem(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id, $chart_id, $dataset_id, $chart_item_id)
    {
        $chart_element = ChartItem::findOrFail($chart_item_id);

        if ($chart_element) {
            $chart_element->delete();
        }

        return redirect()->back()->with('message', ['Das Chart-Element wurde gelöscht.']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateChartItem(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id, $chart_id, $dataset_id, $chart_item_id)
    {
        $chart_element = ChartItem::find($chart_item_id);
        $chart_element->value = $request->value;
        $chart_element->color = $request->color;
        $chart_element->save();

        return redirect()->back()->with('message', ['Das Chart-Element wurde erfolgreich aktualisiert.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateChartLabel(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id, $chart_id, $chart_label_id)
    {
        $chart_label = ChartLabel::find($chart_label_id);
        $chart_label->label = $request->label;
        $chart_label->save();

        return redirect()->back()->with('message', ['Das Chart-Label wurde erfolgreich aktualisiert.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteChartLabel(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id, $chart_id, $chart_label_id)
    {
        $chart_label = ChartLabel::findOrFail($chart_label_id);

        if ($chart_label) {
            $chart_label->delete();
        }

        return redirect()->back()->with('message', ['Das Chart-Label wurde gelöscht.']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id)
    {
        $auth_user = \Auth::user();
        $content_element = null;

        if ($auth_user) {
            $content_element = ContentElement::where('id', '=', $content_element_id)->first();

            if ($content_element->type === 'text') {
                $text = Text::where('content_element_id', '=', $content_element->id)->first();
                if ($text) {
                    $content_element->text = $text->text;
                }
            }

            if ($content_element->type === 'infobox') {
                $text = InfoBox::where('content_element_id', '=', $content_element->id)->first();
                if ($text) {
                    $content_element->text = $text->text;
                    $this->checkForMathShortCodes($content_element);
                }
            }

            if ($content_element->type === 'video') {
                $video = Video::where('content_element_id', '=', $content_element->id)->first();
                if ($video) {
                    $content_element->source = $video->source;
                    $content_element->description = $video->description;
                }
            }

            if ($content_element->type === 'iframe') {
                $iframe = Iframe::where('content_element_id', '=', $content_element->id)->first();
                if ($iframe) {
                    $content_element->source = $iframe->source;
                    $content_element->description = $iframe->description;
                }
            }

            if ($content_element->type === 'image') {
                $image = Image::where('content_element_id', '=', $content_element->id)->first();
                if ($image) {
                    $content_element->image = $image;
                }
            }

            if ($content_element->type === 'quote') {
                $quote = Quote::where('content_element_id', '=', $content_element->id)->with('image')->first();

                if ($quote) {
                    $content_element->text = $quote->text;
                    $content_element->originator = $quote->originator;
                    $content_element->year = $quote->year;
                    if ($quote->image) {
                        $content_element->imagesource = $quote->image->image_original_path;
                    }
                }
            }

            if ($content_element->type === 'chart') {
                $chart = Chart::where('content_element_id', '=', $content_element->id)->with('chartDatasets')->with('chartLabels')->first();
                if ($chart) {
                    if ($chart->chartDatasets) {
                        foreach ($chart->chartDatasets as $chart_dataset) {
                            $chartItems = ChartItem::where('dataset_id', '=', $chart_dataset->id)->get();

                            $chart_dataset->chartItems = $chartItems;
                        }
                    }

                    $content_element->chart = $chart;
                }
            }
        }

        return view('content-element.edit', ['book_id' => $book_id, 'chapter_id' => $chapter_id, 'sub_chapter_id' => $sub_chapter_id, 'learning_section_id' => $learning_section_id, 'content_element' => $content_element]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function checkForMathShortCodes($content_element) {
        if (strpos($content_element->text, '[math]') !== FALSE) {
            preg_match_all('/\[math\]/', $content_element->text,$matches, PREG_OFFSET_CAPTURE);
            $reversed_matches = array_reverse($matches[0]);
            $text_to_handle = $content_element->text;
            $text_parts = [];

            foreach ($reversed_matches as $match => $value) {
                // $content_element->text .= $value[0] . $value[1];
                $till_end_pos = strpos($text_to_handle, '[/math]', $value[1]) - $value[1] + 7;

                if ($till_end_pos > 0) {
                    $to_be_replaced = substr($text_to_handle, $value[1], $till_end_pos);

                    $note_text = substr($to_be_replaced, 6, strlen($to_be_replaced) - 13);

                    $replace = $note_text;
                    $text_to_handle = str_replace($to_be_replaced, $replace, $text_to_handle);
                    $tmp_parts = explode($replace, $text_to_handle);
                    $text_to_handle = $tmp_parts[0];

                    if ($tmp_parts[1] && strlen($tmp_parts[1])) {
                        $text_parts[] = [ 'type' => 'text', 'text' => $tmp_parts[1] ];
                    }
                    $text_parts[] = [ 'type' => 'math', 'text' => $replace ];
                }
            }

            if ($text_to_handle && strlen($text_to_handle)) {
                $text_parts[] = [ 'type' => 'text', 'text' => $text_to_handle ];
            }

            $content_element->textParts = array_reverse($text_parts);
        }
    }

    public function getChartById(Request $request, $chart_id) {
        $chart = Chart::where('id', '=', $chart_id)->with('chartDatasets')->with('chartLabels')->first();
        if ($chart) {
            if ($chart->chartDatasets) {
                foreach ($chart->chartDatasets as $chart_dataset) {
                    $chartItems = ChartItem::where('dataset_id', '=', $chart_dataset->id)->get();

                    $chart_dataset->chartItems = $chartItems;
                }
            }
        }

        return response()->json(['chart' => $chart, 'status' => 'success']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateType(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id)
    {
        $content_element = ContentElement::find($content_element_id);
        $content_element->type = $request->type;
        $content_element->save();

        if ($request->type === 'chart') {
            Chart::create([
                'content_element_id' => $content_element->id
            ]);
        }

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateImageDescription(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id)
    {
        $image = Image::where('content_element_id', '=', $content_element_id)->first();
        $image->description = $request->description;
        $image->size = $request->radioSize;
        $image->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id)
    {
        $content_element = ContentElement::find($content_element_id);

        $text = null;
        if ($content_element && $content_element->type === 'text') {
            $text = Text::where('content_element_id', '=', $content_element->id)->first();

            $givenText = str_replace('<a href="http://#', '<a role="button" class="scroll-to" href="#', $request->text);

            if (!$text) {
                Text::create([
                    'content_element_id' => $content_element->id,
                    'text' => $givenText
                ]);
            } else {
                $text->text = $givenText;
                $text->save();
            }
        }

        if ($content_element && $content_element->type === 'infobox') {
            $infobox = InfoBox::where('content_element_id', '=', $content_element->id)->first();

            if (!$infobox) {
                InfoBox::create([
                    'content_element_id' => $content_element->id,
                    'text' => $request->text
                ]);
            } else {
                $infobox->text = $request->text;
                $infobox->save();
            }
        }

        if ($content_element && $content_element->type === 'chart') {
            $chart = Chart::where('content_element_id', '=', $content_element->id)->first();

            if ($chart) {
                $chart->type = $request->radioType;
                $chart->title = $request->title;
                $chart->description = $request->description;
                $chart->y_axis_min = $request->y_axis_min;
                $chart->y_axis_max = $request->y_axis_max;
                $chart->label_x_axis = $request->label_x_axis;
                $chart->label_y_axis = $request->label_y_axis;
                $chart->save();
            }
        }

        if ($content_element && $content_element->type === 'video') {
            $video = Video::where('content_element_id', '=', $content_element->id)->first();
            if (!$video) {
                Video::create([
                    'content_element_id' => $content_element->id,
                    'source' => $request->source,
                    'description' => $request->description
                ]);
            } else {
                $video->source = $request->source;
                $video->description = $request->description;
                $video->save();
            }
        }

        if ($content_element && $content_element->type === 'iframe') {
            $iframe = Iframe::where('content_element_id', '=', $content_element->id)->first();
            if (!$iframe) {
                Iframe::create([
                    'content_element_id' => $content_element->id,
                    'source' => $request->source,
                    'description' => $request->description
                ]);
            } else {
                $iframe->source = $request->source;
                $iframe->description = $request->description;
                $iframe->save();
            }
        }

        if ($content_element && $content_element->type === 'image') {
            $image = Image::where('content_element_id', '=', $content_element->id)->first();
            if (!$image) {
                Image::create([
                    'content_element_id' => $content_element->id,
                    'source' => $request->source,
                    'description' => $request->description
                ]);
            } else {
                $image->source = $request->source;
                $image->description = $request->description;
                $image->save();
            }
        }

        if ($content_element && $content_element->type === 'quote') {
            $quote = Quote::where('content_element_id', '=', $content_element->id)->first();
            if (!$quote) {
                Quote::create([
                    'content_element_id' => $content_element->id,
                    'text' => $request->text,
                    'originator' => $request->originator,
                    'year' => $request->year
                ]);
            } else {
                $quote->text = $request->text;
                $quote->originator = $request->originator;
                $quote->year = $request->year;
                $quote->save();
            }
        }

        $content_element->is_protected = $request->isProtected === 'on';
        $content_element->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $content_element_id)
    {
        $content_element = ContentElement::findOrFail($content_element_id);

        if ($content_element) {
            $content_element->delete();
        }

        return redirect()->back()->with('message', ['Der Lernabschnitt wurde gelöscht.']);
    }


    /**
     * Upload an image and resize it.
     *
     * @param  int  $book_id
     * @return \Illuminate\Http\Response
     */
    public function uploadChart(Request $request, $chart_id)
    {

        /* $this->validate($request, [

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]); */

        $input['imagename'] = 'chart_' . $chart_id;
        $destinationPath = storage_path('app/public/attachments/');

        // orignal
        $uploaded_image = \Image::make($request->get('imgBase64'))->resize(2200, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $uploaded_image->save($destinationPath.$input['imagename'].'_original.jpg');

        $image_path = URL::to('/').'/storage/'.$input['imagename'];

        return redirect()->back()->with('message', ['Das Bild wurde erfolgreich gespeichert.']);

    }

    /**
     * Upload an image and resize it.
     *
     * @param  int  $book_id
     * @return \Illuminate\Http\Response
     */
    public function uploadMath(Request $request, $content_id)
    {
        /* $this->validate($request, [

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]); */

        $input['imagename'] = 'math_' . $content_id;
        $destinationPath = storage_path('app/public/attachments/');

        // orignal
        $uploaded_image = \Image::make($request->get('imgBase64'))->resize(1200, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $uploaded_image->save($destinationPath.$input['imagename'].'_original.jpg');

        $image_path = URL::to('/').'/storage/'.$input['imagename'];

        return redirect()->back()->with('message', ['Das Bild wurde erfolgreich gespeichert.']);

    }

    /**
     * Upload an image and resize it.
     *
     * @param  int  $book_id
     * @return \Illuminate\Http\Response
     */
    public function contentFileUpload(Request $request, $content_element_id)
    {

        $this->validate($request, [

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $input['imagename'] = time();
        $destinationPath = storage_path('app/public/attachments/');

        // orignal
        $uploaded_image = \Image::make($request->file('image'));
        $uploaded_image->save($destinationPath.$input['imagename'].'_original.jpg');

        $image_path = URL::to('/').'/storage/'.$input['imagename'];
        $image_orignal_infos = [
            'image_path' => $image_path.'_original.jpg',
            'width' => $uploaded_image->width(),
            'height' => $uploaded_image->height(),
            'size' => $uploaded_image->filesize()
        ];

        // resized
        $uploaded_image->resize(1920, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $uploaded_image->save($destinationPath.$input['imagename'].'.jpg');
        $image_infos = [
            'image_path' => $image_path.'.jpg',
            'width' => $uploaded_image->width(),
            'height' => $uploaded_image->height(),
            'size' => $uploaded_image->filesize()
        ];

        // thumbnail
        $uploaded_image->resize(300,200);
        $uploaded_image->save($destinationPath.$input['imagename'].'_300_200.jpg');
        $image_thumb_infos = [
            'image_path' => $image_path.'_300_200.jpg',
            'width' => $uploaded_image->width(),
            'height' => $uploaded_image->height(),
            'size' => $uploaded_image->filesize()
        ];

        $user = \Auth::user();
        $image = null;

        $content_element = ContentElement::findOrFail($content_element_id);
        $image = null;

        if ($user) {
            $image = Image::where('content_element_id', '=', $content_element->id)->first();

            if ($image) {

                $image->image_original_path = $image_orignal_infos['image_path'];
                $image->image_path = $image_infos['image_path'];
                $image->image_thumb_path = $image_thumb_infos['image_path'];
                $image->width_original = $image_orignal_infos['width'];
                $image->width = $image_infos['width'];
                $image->width_thumb = $image_thumb_infos['width'];
                $image->height_original = $image_orignal_infos['height'];
                $image->height = $image_infos['height'];
                $image->height_thumb = $image_thumb_infos['height'];
                $image->file_size_original = $image_orignal_infos['size'];
                $image->file_size = $image_infos['size'];
                $image->file_size_thumb = $image_thumb_infos['size'];
                $image->save();

            } else {

                $image = Image::create([
                    'image_original_path' => $image_orignal_infos['image_path'],
                    'image_path' => $image_infos['image_path'],
                    'image_thumb_path' => $image_thumb_infos['image_path'],
                    'width_original' => $image_orignal_infos['width'],
                    'width' => $image_infos['width'],
                    'width_thumb' => $image_thumb_infos['width'],
                    'height_original' => $image_orignal_infos['height'],
                    'height' => $image_infos['height'],
                    'height_thumb' => $image_thumb_infos['height'],
                    'file_size_original' => $image_orignal_infos['size'],
                    'file_size' => $image_infos['size'],
                    'file_size_thumb' => $image_thumb_infos['size'],
                    'content_element_id' => ($content_element->type === 'quote') ? null : $content_element_id
                ]);
            }
        }

        if ($content_element->type === 'quote') {
            $quote = Quote::where('content_element_id', '=', $content_element->id)->first();

            if (!$quote) {
                Quote::create([
                    'content_element_id' => $content_element->id,
                    'image_id' => $image->id
                ]);
            } else {
                $quote->image_id = $image->id;
                $quote->save();
            }
        }

        return redirect()->back()->with('message', ['Das Bild wurder erfolgreich gespeichert.']);
    }
}
