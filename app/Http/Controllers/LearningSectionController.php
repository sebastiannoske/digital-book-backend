<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\LearningSection;

class LearningSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $book_id, $chapter_id, $sub_chapter_id)
    {
        $learning_section = LearningSection::create([
            'sub_chapter_id' => $sub_chapter_id
        ]);

        return redirect('books/' . $book_id . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id . '/learning-sections/' . $learning_section->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($book_id, $chapter_id, $sub_chapter_id, $learning_section_id)
    {
        $auth_user = \Auth::user();
        $learning_section = null;

        if ($auth_user) {
            $learning_section = LearningSection::where('id', '=', $learning_section_id)->first();
        }

        return view('learning-section.edit', ['book_id' => $book_id, 'chapter_id' => $chapter_id, 'sub_chapter_id' => $sub_chapter_id, 'learning_section' => $learning_section]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editAll($book_id, $chapter_id, $sub_chapter_id, $learning_section_id)
    {
        $auth_user = \Auth::user();
        $learning_section = null;

        if ($auth_user) {
            $learning_section = LearningSection::where('id', '=', $learning_section_id)->with('content')->first();
        }

        return view('learning-section.edit', compact('learning_section'));
    }

    /**
     * Update order of content elements
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOrder(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id)
    {
        $auth_user = \Auth::user();
        $learning_section = null;

        if ($auth_user) {
            $learning_section = LearningSection::where('id', '=', $learning_section_id)->with('content')->first();
        }



        $learning_section->content->each(function($content, $index) use ($request) {
            $myIndex = 0;
            $tempIndex = 0;

            foreach($request->elements as $key => $value) {
                if ($value['id'] === $content->id) {
                    $myIndex = $tempIndex;
                }
                $tempIndex++;
            };
            $content->update(array_only($request->elements[$myIndex], ['sort_order']));
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateLearningSectionRequest $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id)
    {
        $learning_section = LearningSection::find($learning_section_id);
        $learning_section->keywords_enabled = $request->keywordsEnabled === 'on';
        $learning_section->is_learning_section = $request->isLearningSection === 'on';
        if ($learning_section->is_learning_section) {
            $learning_section->title = $request->title;
            $learning_section->introduction = $request->introduction;
        } else {
            $learning_section->title = '';
            $learning_section->introduction = '';
        }
        $learning_section->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($book_id, $chapter_id, $sub_chapter_id, $learning_section_id)
    {
        $learning_section = LearningSection::findOrFail($learning_section_id);

        if ($learning_section) {
            $learning_section->delete();
        }

        return redirect()->back()->with('message', ['Der Lernabschnitt wurde gelöscht.']);
    }
}
