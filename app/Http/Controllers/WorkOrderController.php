<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WorkOrder;

class WorkOrderController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id)
    {
        $work_order = WorkOrder::create([
            'learning_section_id' => $learning_section_id
        ]);

        return redirect('books/' . $book_id . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id . '/learning-sections/' . $learning_section_id . '/work-orders/'. $work_order->id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $work_order_id)
    {
        $auth_user = \Auth::user();
        $work_order = null;

        if ($auth_user) {
            $work_order = WorkOrder::where('id', '=', $work_order_id)->first();

        }

        return view('work-order.edit', ['book_id' => $book_id, 'chapter_id' => $chapter_id, 'sub_chapter_id' => $sub_chapter_id, 'learning_section_id' => $learning_section_id, 'work_order' => $work_order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $work_order_id)
    {
        $work_order = WorkOrder::find($work_order_id);
        $work_order->text = $request->text;
        $work_order->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $work_order_id)
    {
        $work_order = WorkOrder::findOrFail($work_order_id);

        if ($work_order) {
            $work_order->delete();
        }

        return redirect()->back()->with('message', ['Der Arbeitsauftrag wurde gelöscht.']);
    }
}
