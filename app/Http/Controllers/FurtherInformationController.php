<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FurtherInformation;

class FurtherInformationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id)
    {
        $further_information = FurtherInformation::create([
            'learning_section_id' => $learning_section_id
        ]);

        return redirect('books/' . $book_id . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id . '/learning-sections/' . $learning_section_id . '/further-information/'. $further_information->id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $further_information_id)
    {
        $auth_user = \Auth::user();
        $further_information = null;

        if ($auth_user) {
            $further_information = FurtherInformation::where('id', '=', $further_information_id)->first();

        }

        return view('further-information.edit', ['book_id' => $book_id, 'chapter_id' => $chapter_id, 'sub_chapter_id' => $sub_chapter_id, 'learning_section_id' => $learning_section_id, 'further_information' => $further_information]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $further_information_id)
    {
        $further_information = FurtherInformation::find($further_information_id);
        $further_information->text = $request->text;
        $further_information->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $further_information_id)
    {
        $further_information = FurtherInformation::findOrFail($further_information_id);

        if ($further_information) {
            $further_information->delete();
        }

        return redirect()->back()->with('message', ['Die zusätzliche Information wurde gelöscht.']);
    }
}
