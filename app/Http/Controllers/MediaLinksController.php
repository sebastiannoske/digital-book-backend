<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MediaLink;
use App\Attachment;

class MediaLinksController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id)
    {
        $media_link = MediaLink::create([
            'learning_section_id' => $learning_section_id
        ]);

        return redirect('books/' . $book_id . '/chapters/' . $chapter_id . '/sub-chapters/' . $sub_chapter_id . '/learning-sections/' . $learning_section_id . '/media-links/'. $media_link->id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $media_link_id)
    {
        $auth_user = \Auth::user();
        $media_link = null;

        if ($auth_user) {
            $media_link = MediaLink::where('id', $media_link_id)->with('attachment')->first();
        }

        return view('media-link.edit', ['book_id' => $book_id, 'chapter_id' => $chapter_id, 'sub_chapter_id' => $sub_chapter_id, 'learning_section_id' => $learning_section_id, 'media_link' => $media_link]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $media_link_id)
    {
        $media_link = MediaLink::find($media_link_id);
        $media_link->url = $request->url;
        $media_link->display_name = $request->display_name;
        $media_link->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeFile(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $media_link_id)
    {

        if ($request->attachments) {

            $attachment = Attachment::where('media_link_id', $media_link_id)->first();

            if ($attachment) {
                $attachment->delete();
            }


            foreach ($request->attachments as $attachment) {
                $filename = $attachment->store('public/attachments');

                Attachment::create([
                    'media_link_id' => $media_link_id,
                    'filename' => str_replace('public/attachments/', '', $filename),
                    'mimetype' => $attachment->getMimeType()
                ]);
            }

        }

        return response()->json(['status' => 'success', 'message' => 'ok'], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateType(Request $request, $book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $media_link_id)
    {
        $media_link = MediaLink::find($media_link_id);
        $media_link->type = $request->type;
        $media_link->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($book_id, $chapter_id, $sub_chapter_id, $learning_section_id, $media_link_id)
    {
        $media_link = MediaLink::findOrFail($media_link_id);

        if ($media_link) {
            $media_link->delete();
        }

        return redirect()->back()->with('message', ['Der Link wurde gelöscht.']);
    }
}
