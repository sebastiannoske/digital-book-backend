<?php

namespace App\Http\Controllers;

use App\LearningSection;
use App\PageContent;
use Illuminate\Http\Request;
use URL;
use App\Http\Requests;
use App\Book;
use App\Chapter;
use App\Glossary;
use App\ContentElement;
use App\WorkOrder;
use App\MediaLink;
use App\FurtherInformation;
use App\Text;
use App\InfoBox;
use App\Image;
use App\Video;
use App\Iframe;
use App\Quote;
use App\Chart;
use App\ChartItem;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Generate docx file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function generateOdt(Request $request)
    {
        $headers = array(
            'Content-Type'=> 'application/odt'
        );
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $this->generateContent($phpWord, true);

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'ODText');
        $objWriter->save(public_path('/').'Wipo.odt');

        /* $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save(public_path('/').'Wipo.docx'); */

        return response()->download(public_path('Wipo.odt'), 'Wipo.odt', $headers);
    }

    /**
     * Generate docx file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function generatePdf(Request $request)
    {
        $headers = array(
            'Content-Type'=> 'application/pdf'
        );

        \PhpOffice\PhpWord\Settings::setPdfRendererPath('/var/www/html/oer/backend/vendor/dompdf/dompdf');

        // \PhpOffice\PhpWord\Settings::setPdfRendererPath('../vendor/dompdf/dompdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $pdfLibraryName = \PhpOffice\PhpWord\Settings::getPdfRendererName();
        $pdfLibraryPath = \PhpOffice\PhpWord\Settings::getPdfRendererPath();

        // dd(print($pdfLibraryPath . ' * ' . $pdfLibraryName));

        $this->generateContent($phpWord, false);

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'PDF');
        $objWriter->save(public_path('/').'Wipo.pdf');
        return response()->download(public_path('Wipo.pdf'), 'Wipo.pdf', $headers);
    }

    private function generateContent($phpWord, $strip_tags) {
        $sectionStyle = array(
            'orientation' => 'landscape',
            'marginTop' => 600,
            'colsNum' => 1,
        );
        $section = $phpWord->addSection($sectionStyle);

        /* getting content */
        $chapters = explode(",", $_GET['chapters']);
        $print_by_learning_sections = isset($_GET['learningsections']);
        $learning_section_ids = null;

        if ($print_by_learning_sections) {
            $learning_section_ids = explode(",", $_GET['learningsections']);
        }
        $bookParts = $this->getBook(1);

        foreach ($bookParts->chapters as $chapter) {

            if (in_array($chapter->id, $chapters)) {

                $section->addText();
                \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<h3 style="font-weight: bold; font-size: 32px;">' . $this->stripTheTags($chapter->title, $strip_tags) . '</h3>');

                $section->addText($this->stripTheTags($chapter->introduction, $strip_tags));

                /* '<w:br/><w:br/>' */

                if ($chapter->subChapters) {

                    foreach ($chapter->subChapters as $sub_chapter) {

                        if ($sub_chapter->is_sub_chapter) {
                            $section->addText($this->stripTheTags($sub_chapter->title, $strip_tags));
                        }

                        if ($sub_chapter->learningSections) {

                            foreach ($sub_chapter->learningSections as $learningSection) {

                                if ($learningSection->is_learning_section) {

                                    if (!$print_by_learning_sections || ($print_by_learning_sections && $learning_section_ids &&  in_array($learningSection->id, $learning_section_ids))) {

                                        \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<h3 style="font-weight: bold; font-size: 20px;">' . $this->stripTheTags($learningSection->title, $strip_tags) . '</h3>');
                                        $section->addText($this->stripTheTags($learningSection->introduction, $strip_tags));

                                    }

                                }

                                if (!$print_by_learning_sections || ($print_by_learning_sections && $learning_section_ids &&  in_array($learningSection->id, $learning_section_ids))) {

                                    if ($learningSection->content) {

                                        foreach ($learningSection->content as $content_element) {

                                            if ($content_element->type === 'text') {
                                                $section->addText($this->stripTheTags($content_element->text, $strip_tags));
                                            }

                                            if ($content_element->type === 'image' && $content_element->image) {

                                                $image_name = substr($content_element->image->image_path,
                                                    strpos($content_element->image->image_path, '/storage/') + 9,
                                                    strlen($content_element->image->image_path));

                                                //OLD: $section->addImage(storage_path('app/public/attachments/' . $image_name));

                                                \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<img src="' . storage_path('app/public/attachments/' . $image_name ) .'" width="700"/>');

                                                if ($content_element->image->description) {

                                                    \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<span style="display: block; color: #666;">' . $this->stripTheTags($content_element->image->description, $strip_tags) . '</span>');
                                                    \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<span style="display: block;">&nbsp;</span>');
                                                }

                                            }

                                            if ($content_element->type === 'chart') {
                                                //OLD: $section->addImage(storage_path('app/public/attachments/' . 'chart_' . $content_element->chart->id . '_original.jpg' ));

                                                \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<img src="' . storage_path('app/public/attachments/' . 'chart_' . $content_element->chart->id . '_original.jpg' ) .'" width="700"/>');

                                                if ($content_element->chart->description) {

                                                    \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<span style="display: block; color: #666;">' . $this->stripTheTags($content_element->chart->description, $strip_tags) . '</span>');
                                                    \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<span style="display: block;">&nbsp;</span>');
                                                }
                                            }

                                            if ($content_element->type === 'infobox') {
                                                //OLD: $section->addImage(storage_path('app/public/attachments/' . 'chart_' . $content_element->chart->id . '_original.jpg' ));


                                                if ($content_element->text) {

                                                    $section->addText($this->stripTheTags($content_element->text, $strip_tags), [], array('align' => 'center'));

                                                } else if ($content_element->textParts) {

                                                    foreach ($content_element->textParts as $textPart) {

                                                        if ($textPart['type'] === 'text'):

                                                            $section->addText($this->stripTheTags($textPart['text'], $strip_tags), [], array('align' => 'center'));

                                                        elseif ($textPart['type'] === 'math'):

                                                            $section->addImage(
                                                                storage_path('app/public/attachments/' . 'math_' . $content_element->id . '_original.jpg'),
                                                                array(
                                                                    'alignment'=> \PhpOffice\PhpWord\SimpleType\Jc::CENTER,
                                                                    'width' => 300
                                                                )
                                                            );

                                                        endif;
                                                    }
                                                }

                                            }

                                        }

                                    }

                                    if ($learningSection->further_informations && count($learningSection->further_informations)) {

                                        \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<h3 style="font-weight: bold;">Zusatzinformationen:</h3>');

                                        foreach ($learningSection->further_informations as $further_information) {
                                                                                        
                                            \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<span style="display: block;">- ' . $this->stripTheTags($further_information->text, true) . '</span>');

                                            if (strpos($further_information->text, 'href="') !== FALSE) {

                                                preg_match_all('/href="/', $further_information->text,$matches, PREG_OFFSET_CAPTURE);
                                                $added_link = false;

                                                foreach ($matches[0] as $match => $value) {

                                                    $till_end_pos = strpos($further_information->text, '"', $value[1] + 6) - ($value[1] + 6);

                                                    if ($till_end_pos > 0) {
                                                        $to_be_replaced = substr($further_information->text, $value[1] + 6, $till_end_pos);

                                                        \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<span style="display: block;">( ' . $to_be_replaced . ' )</span>');
                                                        $added_link = true;
                                                    }
                                                }

                                                if ($added_link) {
                                                    \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<span style="display: block;">&nbsp;</span>');
                                                }

                                            }

                                        }

                                    }

                                    if ($learningSection->is_learning_section) {

                                        $work_orders = WorkOrder::where('learning_section_id', '=', $learningSection->id)->orderBy('sort_order', 'asc')->get();

                                        if ($work_orders && count($work_orders)) {
                                            \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<h3 style="font-weight: bold;">Arbeitsaufträge:</h3>');

                                            foreach ($work_orders as $work_order) {
                                                \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<span style="display: block;">- ' . $this->stripTheTags($work_order->text, $strip_tags) . '</span>');
                                            }

                                            \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<span style="display: block;">&nbsp;</span>');
                                        }

                                    }

                                }

                            }

                        }

                    }

                }

            }
        }
    }

    private function xmlEntities($str) {
        $xml = array('&#34;','&#38;','&#38;','&#60;','&#62;','&#160;','&#161;','&#162;','&#163;','&#164;','&#165;','&#166;','&#167;','&#168;','&#169;','&#170;','&#171;','&#172;','&#173;','&#174;','&#175;','&#176;','&#177;','&#178;','&#179;','&#180;','&#181;','&#182;','&#183;','&#184;','&#185;','&#186;','&#187;','&#188;','&#189;','&#190;','&#191;','&#192;','&#193;','&#194;','&#195;','&#196;','&#197;','&#198;','&#199;','&#200;','&#201;','&#202;','&#203;','&#204;','&#205;','&#206;','&#207;','&#208;','&#209;','&#210;','&#211;','&#212;','&#213;','&#214;','&#215;','&#216;','&#217;','&#218;','&#219;','&#220;','&#221;','&#222;','&#223;','&#224;','&#225;','&#226;','&#227;','&#228;','&#229;','&#230;','&#231;','&#232;','&#233;','&#234;','&#235;','&#236;','&#237;','&#238;','&#239;','&#240;','&#241;','&#242;','&#243;','&#244;','&#245;','&#246;','&#247;','&#248;','&#249;','&#250;','&#251;','&#252;','&#253;','&#254;','&#255;');
        $html = array('&quot;','&amp;','&amp;','&lt;','&gt;','&nbsp;','&iexcl;','&cent;','&pound;','&curren;','&yen;','&brvbar;','&sect;','&uml;','&copy;','&ordf;','&laquo;','&not;','&shy;','&reg;','&macr;','&deg;','&plusmn;','&sup2;','&sup3;','&acute;','&micro;','&para;','&middot;','&cedil;','&sup1;','&ordm;','&raquo;','&frac14;','&frac12;','&frac34;','&iquest;','&Agrave;','&Aacute;','&Acirc;','&Atilde;','&Auml;','&Aring;','&AElig;','&Ccedil;','&Egrave;','&Eacute;','&Ecirc;','&Euml;','&Igrave;','&Iacute;','&Icirc;','&Iuml;','&ETH;','&Ntilde;','&Ograve;','&Oacute;','&Ocirc;','&Otilde;','&Ouml;','&times;','&Oslash;','&Ugrave;','&Uacute;','&Ucirc;','&Uuml;','&Yacute;','&THORN;','&szlig;','&agrave;','&aacute;','&acirc;','&atilde;','&auml;','&aring;','&aelig;','&ccedil;','&egrave;','&eacute;','&ecirc;','&euml;','&igrave;','&iacute;','&icirc;','&iuml;','&eth;','&ntilde;','&ograve;','&oacute;','&ocirc;','&otilde;','&ouml;','&divide;','&oslash;','&ugrave;','&uacute;','&ucirc;','&uuml;','&yacute;','&thorn;','&yuml;');
        $str = str_replace($html,$xml,$str);
        $str = str_ireplace($html,$xml,$str);
        return $str;
    }

    private function stripTheTags($text, $strip_tags) {
        if ($strip_tags) {
            return htmlspecialchars(strip_tags($this->xmlEntities($text)));
        } else {
            return $text;
        }

    }

    private function getBook($id) {
        $book = Book::find($id)->with('image')->first();
        $chapters = Chapter::where('book_id', '=', $id)->with('subChapters')->orderBy('sort_order', 'asc')->get();
        $book->chapters = $chapters;

        foreach ($book->chapters as $chapter) {
            foreach ($chapter->subChapters as $sub_chapter) {
                $learningSections = LearningSection::where('sub_chapter_id', '=', $sub_chapter->id)->orderBy('sort_order', 'asc')->get();
                $sub_chapter->learningSections = $learningSections;

                foreach ($sub_chapter->learningSections as $learning_section) {
                   $content_elements = ContentElement::where([
                       ['learning_section_id', '=', $learning_section->id],
                       ['is_protected', '=', false]
                   ])->orderBy('sort_order', 'asc')->get();

                    if ($learning_section->is_learning_section) {
                        $work_orders = WorkOrder::where('learning_section_id', '=', $learning_section->id)->orderBy('sort_order', 'asc')->get();
                        $learning_section->workOrders = $work_orders;
                    }

                    if ($content_elements) {
                        foreach ($content_elements as $content_element) {

                            if ($content_element->type === 'text') {
                                $text = Text::where('content_element_id', '=', $content_element->id)->first();
                                if ($text) {
                                    $content_element->text = $text->text;
                                }
                            }

                            if ($content_element->type === 'infobox') {
                                $text = InfoBox::where('content_element_id', '=', $content_element->id)->first();
                                if ($text) {
                                    $content_element->text = $text->text;
                                    $this->checkForMathShortCodes($content_element);
                                }
                            }

                            if ($content_element->type === 'video') {
                                $video = Video::where('content_element_id', '=', $content_element->id)->first();
                                if ($video) {
                                    $content_element->source = $video->source;
                                    $content_element->description = $video->description;
                                }
                            }

                            if ($content_element->type === 'iframe') {
                                $iframe = Iframe::where('content_element_id', '=', $content_element->id)->first();
                                if ($iframe) {
                                    $content_element->source = $iframe->source;
                                    $content_element->description = $iframe->description;
                                }
                            }

                            if ($content_element->type === 'image') {
                                $image = Image::where('content_element_id', '=', $content_element->id)->first();
                                if ($image) {
                                    $content_element->image = $image;
                                }
                            }

                            if ($content_element->type === 'quote') {
                                $quote = Quote::where('content_element_id', '=', $content_element->id)->with('image')->first();

                                if ($quote) {
                                    $content_element->text = $quote->text;
                                    $content_element->originator = $quote->originator;
                                    $content_element->year = $quote->year;
                                    if ($quote->image) {
                                        $content_element->imagesource = $quote->image->image_original_path;
                                    }
                                }
                            }

                            if ($content_element->type === 'chart') {
                                $chart = Chart::where('content_element_id', '=', $content_element->id)->with('chartLabels')->with('chartDatasets')->first();

                                if ($chart) {
                                    if ($chart->chartDatasets) {
                                        foreach ($chart->chartDatasets as $chart_dataset) {
                                            $chartItems = ChartItem::where('dataset_id', '=', $chart_dataset->id)->get();

                                            $chart_dataset->chartItems = $chartItems;
                                        }
                                    }

                                    $content_element->chart = $chart;
                                }
                            }

                            $learning_section->content = $content_elements;
                        }
                    }

                    $further_informations = FurtherInformation::where('learning_section_id', '=', $learning_section->id)->orderBy('sort_order', 'asc')->get();
                    $learning_section->further_informations = $further_informations;
                }
            }
        }

        return $book;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function checkForNoteShortCodes($content_element, $has_notes) {
        if (strpos($content_element->text, '[footnote]') !== FALSE) {
            preg_match_all('/\[footnote\]/', $content_element->text,$matches, PREG_OFFSET_CAPTURE);
            $reversed_matches = array_reverse($matches[0]);
            $given_text = $content_element->text;

            $note_index = sizeof($reversed_matches);

            foreach ($reversed_matches as $match => $value) {
                // $content_element->text .= $value[0] . $value[1];
                $till_end_pos = strpos($given_text, '[/footnote]', $value[1]) - $value[1] + 11;

                if ($till_end_pos > 0) {
                    if (!$has_notes) {
                        $has_notes = true;
                    }
                    $to_be_replaced = substr($given_text, $value[1], $till_end_pos);

                    $note_text = substr($to_be_replaced, 10, strlen($to_be_replaced) - 21);
                    $all_notes[] = [ 'noteIndex' => $note_index, 'text' => $note_text ];

                    $given_text = str_replace($to_be_replaced, "<a class='keyword is-note'><span>" . $note_index . '</span><span>' . $note_text . '<span></span></span></a>', $given_text);
                    $note_index--;
                }
            }

            $content_element->text = $given_text;
        }

        return $has_notes;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function checkForMathShortCodes($content_element) {
        if (strpos($content_element->text, '[math]') !== FALSE) {
            preg_match_all('/\[math\]/', $content_element->text,$matches, PREG_OFFSET_CAPTURE);
            $reversed_matches = array_reverse($matches[0]);
            $text_to_handle = $content_element->text;
            $text_parts = [];

            foreach ($reversed_matches as $match => $value) {
                // $content_element->text .= $value[0] . $value[1];
                $till_end_pos = strpos($text_to_handle, '[/math]', $value[1]) - $value[1] + 7;

                if ($till_end_pos > 0) {
                    $to_be_replaced = substr($text_to_handle, $value[1], $till_end_pos);

                    $note_text = substr($to_be_replaced, 6, strlen($to_be_replaced) - 13);

                    $replace = $note_text;
                    $text_to_handle = str_replace($to_be_replaced, $replace, $text_to_handle);
                    $tmp_parts = explode($replace, $text_to_handle);
                    $text_to_handle = $tmp_parts[0];

                    if ($tmp_parts[1] && strlen($tmp_parts[1])) {
                        $text_parts[] = [ 'type' => 'text', 'text' => $tmp_parts[1] ];
                    }
                    $text_parts[] = [ 'type' => 'math', 'text' => $replace ];
                }
            }

            if ($text_to_handle && strlen($text_to_handle)) {
                $text_parts[] = [ 'type' => 'text', 'text' => $text_to_handle ];
            }

            $content_element->textParts = array_reverse($text_parts);
            $content_element->text = null;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id)->with('image')->first();
        $chapters = Chapter::where('book_id', '=', $id)->with('subChapters')->orderBy('sort_order', 'asc')->get();
        $glossary = Glossary::where('book_id', '=', $id)->with('commentaries')->first();
        $book->chapters = $chapters;
        $keywords = [];

        if ($glossary && $glossary->commentaries) {
            foreach ($glossary->commentaries as $commentary) {
                $keywords[$commentary->glosse] = (strlen($commentary->reference) > 0)
                    ? "<a class='keyword'><span>" . $commentary->glosse . '</span><span>' . $commentary->comment . '<span>Quelle: ' . $commentary->reference . '</span></span></a>'
                    : "<a class='keyword'><span>" . $commentary->glosse . '</span><span>' . $commentary->comment . '<span></span></span></a>';
            }
        }

        foreach ($book->chapters as $chapter) {
            $chapter_keywords = [];

            if ($glossary && $glossary->commentaries) {
                foreach ($glossary->commentaries as $commentary) {
                    $commentary->in_chapter = false;
                }
            }

            foreach ($chapter->subChapters as $sub_chapter) {
                $learningSections = LearningSection::where('sub_chapter_id', '=', $sub_chapter->id)->orderBy('sort_order', 'asc')->get();
                $sub_chapter->learningSections = $learningSections;

                if ($glossary && $glossary->commentaries) {
                    foreach ($glossary->commentaries as $commentary) {
                        $commentary->in_learning_section = false;
                    }
                }

                foreach ($sub_chapter->learningSections as $learning_section) {
                    $learning_section_keywords = [];

                    if ($glossary && $glossary->commentaries && $learning_section->keywords_enabled) {
                        foreach ($glossary->commentaries as $commentary) {
                            if (strpos($learning_section->introduction, $commentary->glosse)) {
                                $commentary->in_chapter = true;
                                $commentary->in_learning_section = true;
                            }
                        }
                        $learning_section->introduction = str_replace(array_keys($keywords), array_values($keywords), $learning_section->introduction);
                    }

                    $all_notes = [];
                    $has_notes = false;
                    $content_elements = ContentElement::where('learning_section_id', '=', $learning_section->id)->orderBy('sort_order', 'asc')->get();

                    if ($learning_section->is_learning_section) {
                        $work_orders = WorkOrder::where('learning_section_id', '=', $learning_section->id)->orderBy('sort_order', 'asc')->get();
                        $learning_section->workOrders = $work_orders;

                        $media_links = MediaLink::where('learning_section_id', '=', $learning_section->id)->with('attachment')->orderBy('sort_order', 'asc')->get();
                        foreach ($media_links as $media_link) {
                            if ($media_link->attachment) {
                                $media_link->attachment->filename = url('/') . "/storage/" . $media_link->attachment->filename;
                            }
                        }
                        $learning_section->mediaLinks = $media_links;
                    }

                    $further_information = FurtherInformation::where('learning_section_id', '=', $learning_section->id)->orderBy('sort_order', 'asc')->get();
                    $learning_section->furtherInformation = $further_information;

                    if ($content_elements) {
                        foreach ($content_elements as $content_element) {
                            $content_element->hasTable = false;

                            if ($content_element->type === 'text') {
                                $text = Text::where('content_element_id', '=', $content_element->id)->first();
                                if ($text) {
                                    if ($glossary && $glossary->commentaries && $learning_section->keywords_enabled) {
                                        foreach ($glossary->commentaries as $commentary) {
                                            if (!$commentary->in_chapter && strpos($text->text, $commentary->glosse)) {
                                                $commentary->in_chapter = true;
                                                $commentary->in_learning_section = true;
                                            }
                                        }

                                        $content_element->text = str_replace(array_keys($keywords), array_values($keywords), $text->text);
                                    } else {
                                        $content_element->text = $text->text;
                                    }

                                    if (strpos($content_element->text, '<table') !== FALSE) {
                                        $content_element->hasTable = true;
                                    }

                                    $has_notes = $this->checkForNoteShortCodes($content_element, $has_notes);
                                    // $this->checkForMathShortCodes($content_element);

                                }
                            }

                            if ($content_element->type === 'infobox') {
                                $text = InfoBox::where('content_element_id', '=', $content_element->id)->first();
                                if ($text) {
                                    $content_element->text = $text->text;
                                    $this->checkForMathShortCodes($content_element);
                                }
                            }

                            if ($content_element->type === 'video') {
                                $video = Video::where('content_element_id', '=', $content_element->id)->first();
                                if ($video) {
                                    $content_element->source = $video->source;
                                    $content_element->description = $video->description;
                                }
                            }

                            if ($content_element->type === 'iframe') {
                                $iframe = Iframe::where('content_element_id', '=', $content_element->id)->first();
                                if ($iframe) {
                                    $content_element->source = $iframe->source;
                                    $content_element->description = $iframe->description;
                                }
                            }

                            if ($content_element->type === 'image') {
                                $image = Image::where('content_element_id', '=', $content_element->id)->first();
                                if ($image) {
                                    $content_element->image = $image;
                                }
                            }

                            if ($content_element->type === 'quote') {
                                $quote = Quote::where('content_element_id', '=', $content_element->id)->with('image')->first();

                                if ($quote) {
                                    $content_element->text = $quote->text;
                                    $content_element->originator = $quote->originator;
                                    $content_element->year = $quote->year;
                                    if ($quote->image) {
                                        $content_element->imagesource = $quote->image->image_original_path;
                                    }
                                }
                            }

                            if ($content_element->type === 'chart') {
                                $chart = Chart::where('content_element_id', '=', $content_element->id)->with('chartLabels')->with('chartDatasets')->first();

                                if ($chart) {
                                    if ($chart->chartDatasets) {
                                        foreach ($chart->chartDatasets as $chart_dataset) {
                                            $chartItems = ChartItem::where('dataset_id', '=', $chart_dataset->id)->get();

                                            $chart_dataset->chartItems = $chartItems;
                                        }
                                    }

                                    $content_element->chart = $chart;
                                }
                            }

                            $learning_section->content = $content_elements;
                        }
                    }

                    if ($has_notes) {
                        $learning_section->notes = $all_notes;
                    }


                    if ($glossary && $glossary->commentaries) {
                        foreach ($glossary->commentaries as $commentary) {
                            if ($commentary->in_learning_section) {
                                $learning_section_keywords[] = [
                                    'keyword' => $commentary->glosse,
                                    'comment' => $commentary->comment,
                                    'reference' => $commentary->reference
                                ];
                            }
                        }
                    }

                    $learning_section->keywords = $learning_section_keywords;
                }
            }


            if ($glossary && $glossary->commentaries) {
                foreach ($glossary->commentaries as $commentary) {
                    if ($commentary->in_chapter) {
                        $chapter_keywords[] = [
                            'keyword' => $commentary->glosse,
                            'comment' => $commentary->comment,
                            'reference' => $commentary->reference
                        ];
                    }
                }
            }

            $chapter->keywords = $chapter_keywords;
        }

        return response()->json(['book' => $book, 'status' => 'success', 'total' => $book->count()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $auth_user = \Auth::user();
        $book = null;

        if ($auth_user) {
            $book = Book::where('id', '=', $id)->with('image')->with('glossary')->with('chapters')->with('pageContents')->first();
        }

        return view('edit-book', ['book' => $book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateBookRequest $request, $id)
    {
        $book = Book::find($id);
        $book->title = $request->title;
        $book->author = $request->author;
        $book->isbn = $request->isbn;
        $book->color = $request->color;
        $book->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPageContent($book_id, $page_content_id)
    {
        $auth_user = \Auth::user();
        $page_content = null;

        if ($auth_user) {
            $page_content = PageContent::where('id', '=', $page_content_id)->first();
        }

        return view('page-content.edit', ['book_id' => $book_id, 'page_content' => $page_content]);
    }

    public function getInprintText($book_id) {
        $page_content = PageContent::where('book_id', '=', $book_id)->where('slug', '=', 'impressum')->first();
        return response()->json(['content' => $page_content, 'status' => 'success']);
    }

    public function getPrivacyText($book_id) {
        $page_content = PageContent::where('book_id', '=', $book_id)->where('slug', '=', 'datenschutz')->first();
        return response()->json(['content' => $page_content, 'status' => 'success']);
    }

    public function getCopyrightText($book_id) {
        $page_content = PageContent::where('book_id', '=', $book_id)->where('slug', '=', 'copyright')->first();
        return response()->json(['content' => $page_content, 'status' => 'success']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePageContent(Request $request, $book_id, $page_content_id)
    {
        $page_content = PageContent::find($page_content_id);
        $page_content->text = $request->text;
        $page_content->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);

    }

    /**
     * Update order of content elements
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateChapterOrder(Request $request, $book_id)
    {
        $auth_user = \Auth::user();
        $book = null;

        if ($auth_user) {
            $book = Book::where('id', '=', $book_id)->with('chapters')->first();
        }



        $book->chapters->each(function($chapters, $index) use ($request) {
            $myIndex = 0;
            $tempIndex = 0;

            foreach($request->elements as $key => $value) {
                if ($value['id'] === $chapters->id) {
                    $myIndex = $tempIndex;
                }
                $tempIndex++;
            };
            $chapters->update(array_only($request->elements[$myIndex], ['sort_order']));
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Upload an image and resize it.
     *
     * @param  int  $book_id
     * @return \Illuminate\Http\Response
     */
    public function fileUpload(Request $request, $book_id)
    {

        $this->validate($request, [

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $input['imagename'] = time();
        $destinationPath = storage_path('app/public/attachments/');

        // orignal
        $uploaded_image = \Image::make($request->file('image'));
        $uploaded_image->save($destinationPath.$input['imagename'].'_original.jpg');

        $image_path = URL::to('/').'/storage/'.$input['imagename'];
        $image_orignal_infos = [
            'image_path' => $image_path.'_original.jpg',
            'width' => $uploaded_image->width(),
            'height' => $uploaded_image->height(),
            'size' => $uploaded_image->filesize()
        ];

        // resized
        $uploaded_image->resize(1920, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $uploaded_image->save($destinationPath.$input['imagename'].'.jpg');
        $image_infos = [
            'image_path' => $image_path.'.jpg',
            'width' => $uploaded_image->width(),
            'height' => $uploaded_image->height(),
            'size' => $uploaded_image->filesize()
        ];

        // thumbnail
        $uploaded_image->resize(300,200);
        $uploaded_image->save($destinationPath.$input['imagename'].'_300_200.jpg');
        $image_thumb_infos = [
            'image_path' => $image_path.'_300_200.jpg',
            'width' => $uploaded_image->width(),
            'height' => $uploaded_image->height(),
            'size' => $uploaded_image->filesize()
        ];

        $user = \Auth::user();
        $image = null;

        if ($user) {

            $book = Book::find($book_id);

            if ($book->image_id) { // already there

                $image = Image::where('id', '=', $book->image_id)->first();
                $image->image_original_path = $image_orignal_infos['image_path'];
                $image->image_path = $image_infos['image_path'];
                $image->image_thumb_path = $image_thumb_infos['image_path'];
                $image->width_original = $image_orignal_infos['width'];
                $image->width = $image_infos['width'];
                $image->width_thumb = $image_thumb_infos['width'];
                $image->height_original = $image_orignal_infos['height'];
                $image->height = $image_infos['height'];
                $image->height_thumb = $image_thumb_infos['height'];
                $image->file_size_original = $image_orignal_infos['size'];
                $image->file_size = $image_infos['size'];
                $image->file_size_thumb = $image_thumb_infos['size'];
                $image->save();

            } else { // create new one

                $image = Image::create([
                    'image_original_path' => $image_orignal_infos['image_path'],
                    'image_path' => $image_infos['image_path'],
                    'image_thumb_path' => $image_thumb_infos['image_path'],
                    'width_original' => $image_orignal_infos['width'],
                    'width' => $image_infos['width'],
                    'width_thumb' => $image_thumb_infos['width'],
                    'height_original' => $image_orignal_infos['height'],
                    'height' => $image_infos['height'],
                    'height_thumb' => $image_thumb_infos['height'],
                    'file_size_original' => $image_orignal_infos['size'],
                    'file_size' => $image_infos['size'],
                    'file_size_thumb' => $image_thumb_infos['size']
                ]);

                $book->image_id = $image->id;
            }

            $book->save();

        }

        return redirect()->back()->with('message', ['Das Bild wurde erfolgreich gespeichert.']);

    }
}
