<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Glossary;
use App\Commentary;

class GlossaryController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($book_id, $glossary_id)
    {
        $auth_user = \Auth::user();
        $glossary = null;

        if ($auth_user) {
            $glossary = Glossary::where('id', '=', $glossary_id)->with('commentaries')->first();
        }

        return view('glossary.edit', ['book_id' => $book_id, 'glossary' => $glossary]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $book_id)
    {
        $glossary = Glossary::create([
            'book_id' => $book_id
        ]);



        return redirect('books/' . $book_id . '/glossaries/' . $glossary->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($book_id, $glossary_id, $commentary_id)
    {
        $commentary = Commentary::findOrFail($commentary_id);


        if ($commentary) {
            $commentary->delete();
        }

        return redirect()->back()->with('message', ['Glosse erfolgreich gelöscht..']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeGlosse(Request $request, $book_id, $glossary_id)
    {
        $commentary = Commentary::create([
            'glossary_id' => $glossary_id
        ]);

        return redirect('books/' . $book_id . '/glossaries/' . $glossary_id . '/commentaries/' . $commentary->id);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editGlosse($book_id, $glossary_id, $commentary_id)
    {
        $auth_user = \Auth::user();
        $commentary = null;

        if ($auth_user) {
            $commentary = Commentary::where('id', '=', $commentary_id)->first();
        }

        return view('glossary.commentary.edit', ['book_id' => $book_id, 'glossary_id' => $glossary_id, 'commentary' => $commentary]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateGlosse(Requests\UpdateCommentaryRequest $request, $book_id, $glossary_id, $commentary_id)
    {
        $comment = Commentary::find($commentary_id);
        $comment->glosse = $request->glosse;
        $comment->comment = $request->comment;
        $comment->reference = $request->reference;
        $comment->save();

        return redirect()->back()->with('message', ['Änderungen gespeichert.']);
    }
}
