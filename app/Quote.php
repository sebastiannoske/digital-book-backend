<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['text', 'originator', 'year', 'image_id', 'content_element_id'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['created_at', 'updated_at', 'content_element_id'];

    /**
     *  Get contentElement associated with the text
     */

    public function contentElement() {

        return $this->belongsTo('App\ContentElement');

    }

    /**
     *  Get image associated with the book
     */

    public function image() {

        return $this->belongsTo('App\Image');

    }
}
