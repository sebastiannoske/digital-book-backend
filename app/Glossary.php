<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Glossary extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['book_id'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['book_id', 'created_at', 'updated_at', 'id'];

    /**
     *  Get book associated with the glassary
     */

    public function book() {

        return $this->belongsTo('App\Book');

    }

    /**
     *  Get learnin sections associated with the chapter
     */

    public function commentaries() {

        return $this->hasMany('App\Commentary');

    }
}
