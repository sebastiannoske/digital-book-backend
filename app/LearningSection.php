<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LearningSection extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['title', 'introduction', 'is_learning_section', 'keywords_enabled', 'sub_chapter_id', 'sort_order'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['created_at', 'updated_at', 'sub_chapter_id'];

    /**
     *  Get subchapter associated with the learning section
     */

    public function subChapter() {

        return $this->belongsTo('App\SubChapter');

    }

    /**
     *  Get content elements associated with the learning section
     */

    public function content() {

        return $this->hasMany('App\ContentElement')->orderBy('sort_order', 'asc');

    }

    /**
     *  Get work orders associated with the learning section
     */

    public function workOrders() {

        return $this->hasMany('App\WorkOrder')->orderBy('sort_order', 'asc');

    }

    /**
     *  Get external media link url associated with the learning section
     */

    public function mediaLinks() {

        return $this->hasMany('App\MediaLink')->orderBy('sort_order', 'asc');

    }

    /**
     *  Get further information associated with the learning section
     */

    public function furtherInformation() {

        return $this->hasMany('App\FurtherInformation')->orderBy('sort_order', 'asc');

    }
}
