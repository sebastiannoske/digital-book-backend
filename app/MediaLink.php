<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaLink extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['url', 'display_name', 'learning_section_id', 'sort_order'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = ['created_at', 'updated_at', 'learning_section_id'];

    /**
     *  Get learningSection associated with the content element
     */

    public function learningSection() {

        return $this->belongsTo('App\LearningSection');

    }

    /**
     *  Get attachments associated with the Media-Link
     */

    public function attachment() {

        return $this->hasOne('App\Attachment');

    }
}
