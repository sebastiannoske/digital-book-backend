# Getting started

## Installation

Clone the repository

`git clone git@gitlab.com:sebastiannoske/digital-book-backend.git`

Switch to the repo folder

Get composer if not installed

(https://getcomposer.org/)

Install all the dependencies using composer

`composer install`

Copy the example env file and make the required configuration changes in the .env file

`cp .env.example .env`

Run the database migrations (Set the database connection in .env before migrating)

`php artisan migrate`


# Requirements

## Laravel 5.6

Find server requirements, installation notes and documentations here

(https://laravel.com/docs/5.6/installation#server-requirements)

# Backup

If you want to backup your application you need


*  a dump of your database ( mysql )
*  a copy of the storage folder which contains all uploaded files
*  a copy of your .env file ( you may change the database credentials, if you will move to another server )
