<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('books', 'BookController', [
    'only' => [
        'index', 'show'
    ]
]);

Route::get('/books/{book_id}/impressum', 'BookController@getInprintText');
Route::get('/books/{book_id}/datenschutz', 'BookController@getPrivacyText');
Route::get('/books/{book_id}/copyright', 'BookController@getCopyrightText');
Route::get('/chart/{chart_id}', 'ContentELementController@getChartById');


/* Route::get('/books/{book_id}/generate-docx', 'BookController@generateDocx'); */
