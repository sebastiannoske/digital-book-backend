<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/books/{book_id}/generate-odt', 'BookController@generateOdt'); // TODO: move to api route
Route::get('/books/{book_id}/generate-pdf', 'BookController@generatePdf'); // TODO: move to api route

Route::get('/books/{book_id}', 'BookController@edit');
Route::post('/books/{book_id}/update', 'BookController@update');
Route::post('/books/{book_id}/updateOrder', 'BookController@updateChapterOrder');
Route::post('/fileUpload/{book_id}', ['as'=>'fileUpload','uses'=>'BookController@fileUpload']);
Route::post('/upload-chart/{chart_id}', ['as'=>'uploadChart','uses'=>'ContentElementController@uploadChart']);
Route::post('/upload-math/{content_id}', ['as'=>'uploadMath','uses'=>'ContentElementController@uploadMath']);

Route::get('/books/{book_id}/page-contents/{page_content_id}', 'BookController@editPageContent');
Route::post('/books/{book_id}/page-contents/{page_content_id}/update', 'BookController@updatePageContent');

Route::get('/books/{book_id}/chapters/{chapter_id}', 'ChapterController@edit');
Route::post('/books/{book_id}/chapters/store', 'ChapterController@store');
Route::post('/books/{book_id}/chapters/{chapter_id}/delete', 'ChapterController@destroy');
Route::post('/books/{book_id}/chapters/{chapter_id}/update', 'ChapterController@update');
Route::post('/books/{book_id}/chapters/{chapter_id}/updateOrder', 'ChapterController@updateSubChapterOrder');

Route::get('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/edit', 'LearningSectionController@editAll');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/updateOrder', 'LearningSectionController@updateOrder');

Route::get('/books/{book_id}/glossaries/{glossary_id}', 'GlossaryController@edit');
Route::post('/books/{book_id}/glossaries/store', 'GlossaryController@store');
Route::post('/books/{book_id}/glossaries/{glossary_id}/commentaries/store', 'GlossaryController@storeGlosse');
Route::get('/books/{book_id}/glossaries/{glossary_id}/commentaries/{commentary_id}', 'GlossaryController@editGlosse');
Route::post('/books/{book_id}/glossaries/{glossary_id}/commentaries/{commentary_id}/delete', 'GlossaryController@destroy');
Route::post('/books/{book_id}/glossaries/{glossary_id}/commentaries/{commentary_id}/update', 'GlossaryController@updateGlosse');

Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/store', 'ChapterController@storeSubChapter');
Route::get('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}', 'ChapterController@editSubChapter');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/delete', 'ChapterController@destroySubChapter');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/update', 'ChapterController@updateSubChapter');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/updateOrder', 'ChapterController@updateLearningSectionOrder');

Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/store', 'LearningSectionController@store');
Route::get('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}', 'LearningSectionController@edit');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/delete', 'LearningSectionController@destroy');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/update', 'LearningSectionController@update');

Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/work-orders/store', 'WorkOrderController@store');
Route::get('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/work-orders/{work_order_id}', 'WorkOrderController@edit');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/work-orders/{work_order_id}/delete', 'WorkOrderController@destroy');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/work-orders/{work_order_id}/update', 'WorkOrderController@update');

Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/media-links/store', 'MediaLinksController@store');
Route::get('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/media-links/{media_link_id}', 'MediaLinksController@edit');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/media-links/{media_link_id}/delete', 'MediaLinksController@destroy');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/media-links/{media_link_id}/update', 'MediaLinksController@update');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/media-links/{media_link_id}/updatetype', 'MediaLinksController@updateType');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/media-links/{media_link_id}/storeFile', 'MediaLinksController@storeFile');


Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/further-information/store', 'FurtherInformationController@store');
Route::get('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/further-information/{further_information_id}', 'FurtherInformationController@edit');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/further-information/{further_information_id}/delete', 'FurtherInformationController@destroy');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/further-information/{further_information_id}/update', 'FurtherInformationController@update');

Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/store', 'ContentElementController@store');
Route::get('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}', 'ContentElementController@edit');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/delete', 'ContentElementController@destroy');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/updatetype', 'ContentElementController@updateType');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/updateImageDescription', 'ContentElementController@updateImageDescription');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/update', 'ContentElementController@update');

Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/charts/{chart_id}/storelabel', 'ContentElementController@storeChartLabel');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/charts/{chart_id}/chart-labels/{chart_label_id}/update', 'ContentElementController@updateChartLabel');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/charts/{chart_id}/chart-labels/{chart_label_id}/delete', 'ContentElementController@deleteChartLabel');

Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/charts/{chart_id}/store', 'ContentElementController@storeChartDataset');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/charts/{chart_id}/datasets/{dataset_id}/update', 'ContentElementController@updateDatasetLabel');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/charts/{chart_id}/datasets/{dataset_id}/delete', 'ContentElementController@deleteDataset');

Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/charts/{chart_id}/datasets/{dataset_id}/store', 'ContentElementController@storeChartItem');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/charts/{chart_id}/datasets/{dataset_id}/chart-items/{chart_item_id}/update', 'ContentElementController@updateChartItem');
Route::post('/books/{book_id}/chapters/{chapter_id}/sub-chapters/{sub_chapter_id}/learning-sections/{learning_section_id}/content-element/{content_element_id}/charts/{chart_id}/datasets/{dataset_id}/chart-items/{chart_item_id}/delete', 'ContentElementController@deleteChartItem');


Route::post('/contentFileUpload/{content_element_id}', ['as'=>'contentFileUpload','uses'=>'ContentElementController@contentFileUpload']);

Route::get('/storage/{filename}', function ($filename)
{
    // dd($filename);
    $path = storage_path('app/public/attachments/' . $filename);


    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/download/{filename}', function ($filename)
{
    // dd($filename);
    $path = storage_path('app/public/attachments/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return response()->download($response);
});